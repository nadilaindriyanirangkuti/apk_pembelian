import 'package:intl/intl.dart';

extension DateTimeExtension on DateTime {
  String get fullDate {
    return DateFormat("dd MMMM yyyy HH.mm", 'id').format(this);
  }
}
