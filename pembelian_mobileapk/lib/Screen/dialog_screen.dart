import 'package:flutter/material.dart';

class DialogScreen extends StatelessWidget {
  final bool isSuccess;
  final String message;
  const DialogScreen({
    super.key,
    required this.isSuccess,
    required this.message,
  });

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
      child: Container(
        color: Colors.transparent,
        height: 200.0,
        width: 320.0,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              message,
              style: const TextStyle(fontSize: 22, fontWeight: FontWeight.bold),
            ),
            const SizedBox(height: 10),
            Padding(
              padding: const EdgeInsets.only(right: 10),
              child: Image.asset(
                isSuccess
                    ? 'assets/image/Berhasil.png'
                    : 'assets/image/Gagal.png',
                width: 150,
              ),
            ),
            const SizedBox(height: 20),
            // Row(
            //   mainAxisAlignment: MainAxisAlignment.center,
            //   children: [
            //     Text(
            //       'Sisa saldo :',
            //       style: TextStyle(fontSize: 16, fontWeight: FontWeight.w400),
            //     ),
            //     SizedBox(
            //       width: 10,
            //     ),
            //     Text(
            //       'Rp. 500.000.000',
            //       style: TextStyle(fontSize: 16, fontWeight: FontWeight.w400),
            //     ),
            //   ],
            // )

            // Text(
            //   'Pembayaran Gagal',
            //   style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold),
            // ),
            // SizedBox(
            //   height: 10,
            // ),
            // Padding(
            //   padding: EdgeInsets.only(right: 10),
            //     // padding: EdgeInsets.all(10.0),
            //     child: Image.asset(
            //       'assets/image/Gagal.png',
            //       width: 150,
            //     )),
          ],
        ),
      ),
    );

    //   child: Container(
    //     decoration: BoxDecoration(
    //       color: Colors.white,
    //       borderRadius: BorderRadius.circular(20),
    //     ),
    //     child: Column(
    //       mainAxisSize: MainAxisSize.min,
    //       children: [
    // Image.asset('assets/image/splash_screen.png'),

    //       ],

    //     ),
    //   ),
  }
}
