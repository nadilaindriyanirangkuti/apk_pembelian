import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:pembelian_mobileapk/Moduls/product_provider.dart';
import 'package:pembelian_mobileapk/Screen/check_rekening_screen.dart';
import 'package:pembelian_mobileapk/Screen/dialog_screen.dart';
import 'package:pembelian_mobileapk/Util/string_util.dart';
import 'package:pembelian_mobileapk/Widget/typography_widget.dart';
import 'package:provider/provider.dart';

class CheckoutPage extends StatelessWidget {
  final String norek;
  final String namaBank;
  const CheckoutPage({
    super.key,
    this.norek = '10016123456',
    this.namaBank = 'BCA',
  });

  // confirmationItems() {
  //   AwesomeDialog(
  //       context: context,
  //       dialogType: DialogType.info,
  //       animType: AnimType.rightSlide,
  //       title: 'Dialog Titleee',
  //       btnCancelOnPress: () {},
  //       btnOkOnPress: () {},
  //       customHeader: Container(
  //         child: Image.asset('assets/image/splash_screen.png'),
  //       ))
  //     ..show();
  // }

  @override
  Widget build(BuildContext context) {
    final productProvider =
        Provider.of<ProductProvider>(context, listen: false);

    return Scaffold(
        appBar: AppBar(
          scrolledUnderElevation: 0,
          centerTitle: true,
          elevation: 0,
          title: Image.asset(
            'assets/image/appbar.png',
            width: 130,
            fit: BoxFit.fill,
            // height: 170,
          ),
        ),
        body: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              padding: EdgeInsets.all(20),
              child: Row(
                children: [
                  ClipRRect(
                    borderRadius: BorderRadius.circular(6.0),
                    child: Image.asset(
                      'assets/image/${productProvider.product['image']}',
                      fit: BoxFit.cover,
                      width: 150,
                      height: 150,
                    ),
                  ),
                  SizedBox(
                    width: 16,
                  ),
                  Expanded(
                      child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        productProvider.product['nama'],
                        style: const TextStyle(
                            fontSize: 18, fontWeight: FontWeight.w600),
                      ),
                      const SizedBox(height: 8),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            (productProvider.product['harga'] as int).currency,
                            style: const TextStyle(
                              fontSize: 17,
                              fontWeight: FontWeight.bold,
                              color: Color(0xFFFF6B00),
                            ),
                          ),
                          Text(
                            'x${productProvider.product['jumlah']}',
                            style: const TextStyle(
                              fontSize: 17,
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(height: 12),
                    ],
                  )),
                ],
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              margin: const EdgeInsets.only(left: 20),
              child: const Text(
                'Ringkasan Pembayaran',
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.w700),
              ),
            ),
            const Divider(
              indent: 20,
              endIndent: 20,
            ),
            const SizedBox(
              height: 30,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const Expanded(
                    child: Text(
                      'Total Harga',
                      style: TextStyle(fontSize: 20),
                    ),
                  ),
                  Text(
                    (productProvider.product['total_harga'] as int).currency,
                    style: const TextStyle(fontSize: 20),
                  )
                ],
              ),
            ),
            SizedBox(
              height: 5,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const Expanded(
                    child: Text(
                      'Ongkos Kirim',
                      style: TextStyle(fontSize: 20),
                    ),
                  ),
                  Text(
                    (productProvider.product['ongkir'] as int).currency,
                    style: const TextStyle(fontSize: 20),
                  )
                ],
              ),
            ),
            SizedBox(
              height: 70,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const Expanded(
                    child: Text(
                      'Total Pembayaran',
                      style: TextStyle(fontSize: 20),
                    ),
                  ),
                  Text(
                    (productProvider.product['total_bayar'] as int).currency,
                    style: const TextStyle(fontSize: 20),
                  )
                ],
              ),
            ),
            SizedBox(
              height: 30,
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              height: 60,
              padding: const EdgeInsets.only(left: 15, right: 15),
              margin: const EdgeInsets.symmetric(vertical: 8, horizontal: 10),
              decoration: BoxDecoration(
                  color: const Color(0xffDED6F1),
                  boxShadow: const [
                    BoxShadow(
                      blurRadius: 0,
                    )
                  ],
                  borderRadius: BorderRadius.circular(10)),
              child: Row(
                children: [
                  InkWell(
                    onTap: () {},
                    child: Container(
                      margin: const EdgeInsets.all(10),
                      child: Image.asset(
                        'assets/image/${namaBank}.png',
                        width: 70,
                        height: 100,
                      ),
                    ),
                  ),
                  const SizedBox(
                    width: 130,
                  ),
                  Container(
                    child: Text(
                      norek,
                      style: TextStyle(fontSize: 18),
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
        bottomNavigationBar: Container(
          width: 160,
          height: 50,
          margin: const EdgeInsets.only(left: 200, bottom: 10, right: 10),
          child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                  backgroundColor: const Color(0xff2E2977),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10))),
              onPressed: () {
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (BuildContext context) => CheckRekeningScreen(
                      norekTujuan: norek,
                    ),
                  ),
                );
              },
              child: const Text(
                'Bayar Sekarang',
                style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.bold,
                    color: Colors.white),
              )),
        ));
  }
}
