import 'package:flutter/material.dart';
import 'package:pembelian_mobileapk/Screen/check_saldo_screen.dart';
import 'package:pembelian_mobileapk/Screen/payment_screen.dart';
import 'package:pembelian_mobileapk/Screen/top_up_saldo_screen.dart';
import 'package:pembelian_mobileapk/DetailProduct/headphone_detail.dart';
import 'package:pembelian_mobileapk/DetailProduct/imac_detail.dart';
import 'package:pembelian_mobileapk/DetailProduct/ipad_detail.dart';
import 'package:pembelian_mobileapk/DetailProduct/iphone_detail.dart';
import 'package:pembelian_mobileapk/DetailProduct/keyboard_detail.dart';
import 'package:pembelian_mobileapk/DetailProduct/mouse_detail.dart';
import 'package:pembelian_mobileapk/DetailProduct/proyektor_detail.dart';
import 'package:pembelian_mobileapk/DetailProduct/detail_screen.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  static const routeName = '/list-product';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: 0,
          surfaceTintColor: Colors.white,
          forceMaterialTransparency: true,
          scrolledUnderElevation: 0.0,
          centerTitle: true,
          title: Image.asset(
            'assets/image/appbar.png',
            width: 130,
            fit: BoxFit.fill,
          ),
        ),
        bottomNavigationBar: BottomNavigationBar(
          backgroundColor: Colors.white,
          currentIndex: 0,
          onTap: (index) {
            // Mengubah halaman yang aktif saat item bottom navigation diubah
            if (index == 2) {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (BuildContext context) => TopUpSaldoScreen(),
                ),
              );
            } else if (index == 1) {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (BuildContext context) => CheckSaldoScreen(),
                ),
              );
            }
          },
          items: const [
            BottomNavigationBarItem(
              icon: Icon(Icons.production_quantity_limits),
              label: 'Product',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.monetization_on),
              label: 'Cek Saldo',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.wallet),
              label: 'Top Up',
            ),
          ],
        ),
        body: GridView.count(
          crossAxisCount: 2,
          childAspectRatio: 0.70,
          physics: const ScrollPhysics(),
          shrinkWrap: true,
          children: [
            Container(
              padding: const EdgeInsets.only(left: 15, right: 15),
              margin: const EdgeInsets.symmetric(vertical: 8, horizontal: 10),
              decoration: BoxDecoration(
                  color: Colors.white,
                  boxShadow: const [
                    BoxShadow(
                      color: Color(0x25413368),
                      offset: Offset(0, 3),
                      blurRadius: 6,
                    )
                  ],
                  borderRadius: BorderRadius.circular(20)),
              child: Column(
                children: [
                  InkWell(
                    onTap: () {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (BuildContext context) =>
                              const DetailPageScreen()));
                    },
                    child: Container(
                      margin: const EdgeInsets.all(10),
                      child: Image.asset(
                        'assets/image/macbook.png',
                        width: 120,
                        height: 100,
                      ),
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.only(bottom: 8),
                    alignment: Alignment.centerLeft,
                    child: const Text(
                      'MacBook',
                      style: TextStyle(
                          fontSize: 18,
                          color: Color(0xFF4C53A5),
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  Container(
                    alignment: Alignment.centerLeft,
                    child: const Text(
                      'Apple MacBook Air (13.6 inci, M2, 2022)...',
                      style: TextStyle(
                        fontSize: 15,
                      ),
                    ),
                  ),
                  const Padding(
                    padding: EdgeInsets.symmetric(vertical: 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          'Rp. 14.999.000',
                          style: TextStyle(
                              fontSize: 15, fontWeight: FontWeight.bold),
                        ),
                        Icon(
                          Icons.shopping_cart_checkout,
                          size: 17,
                          color: Color(0xFF4C53A5),
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
            Container(
              padding: const EdgeInsets.only(left: 15, right: 15),
              margin: const EdgeInsets.symmetric(vertical: 8, horizontal: 10),
              decoration: BoxDecoration(
                  color: Colors.white,
                  boxShadow: const [
                    BoxShadow(
                      color: Color(0x25413368),
                      offset: Offset(0, 3),
                      blurRadius: 6,
                    )
                  ],
                  borderRadius: BorderRadius.circular(20)),
              child: Column(
                children: [
                  InkWell(
                    onTap: () {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (BuildContext context) =>
                              const HeadphoneDetail()));
                    },
                    child: Container(
                      margin: const EdgeInsets.all(10),
                      child: Image.asset(
                        'assets/image/headphone.png',
                        width: 100,
                        height: 100,
                      ),
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.only(bottom: 8),
                    alignment: Alignment.centerLeft,
                    child: const Text(
                      'Headphone',
                      style: TextStyle(
                          fontSize: 20,
                          color: Color(0xFF4C53A5),
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  Container(
                    alignment: Alignment.centerLeft,
                    child: const Text(
                      'Baseus Bowie H1 Noise-Collecion Wireless/...',
                      style: TextStyle(
                        fontSize: 15,
                      ),
                    ),
                  ),
                  const Padding(
                    padding: EdgeInsets.symmetric(vertical: 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          'Rp. 500.000',
                          style: TextStyle(
                              fontSize: 15, fontWeight: FontWeight.bold),
                        ),
                        Icon(
                          Icons.shopping_cart_checkout,
                          size: 17,
                          color: Color(0xFF4C53A5),
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
            Container(
              padding: const EdgeInsets.only(left: 15, right: 15),
              margin: const EdgeInsets.symmetric(vertical: 8, horizontal: 10),
              decoration: BoxDecoration(
                  color: Colors.white,
                  boxShadow: const [
                    BoxShadow(
                      color: Color(0x25413368),
                      offset: Offset(0, 3),
                      blurRadius: 6,
                    )
                  ],
                  borderRadius: BorderRadius.circular(20)),
              child: Column(
                children: [
                  InkWell(
                    onTap: () {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (BuildContext context) =>
                              const IphoneDetail()));
                    },
                    child: Container(
                      margin: const EdgeInsets.all(10),
                      child: Image.asset(
                        'assets/image/iphone.png',
                        width: 100,
                        height: 100,
                      ),
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.only(bottom: 8),
                    alignment: Alignment.centerLeft,
                    child: const Text(
                      'Iphone',
                      style: TextStyle(
                          fontSize: 20,
                          color: Color(0xFF4C53A5),
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  Container(
                    alignment: Alignment.centerLeft,
                    child: const Text(
                      'APPLE IPHONE 13 Pro Max 128GB...',
                      style: TextStyle(
                        fontSize: 16,
                      ),
                    ),
                  ),
                  const Padding(
                    padding: EdgeInsets.symmetric(vertical: 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          'Rp. 17.000.000',
                          style: TextStyle(
                              fontSize: 15, fontWeight: FontWeight.bold),
                        ),
                        Icon(
                          Icons.shopping_cart_checkout,
                          size: 17,
                          color: Color(0xFF4C53A5),
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
            Container(
              padding: const EdgeInsets.only(left: 15, right: 15),
              margin: const EdgeInsets.symmetric(vertical: 8, horizontal: 10),
              decoration: BoxDecoration(
                  color: Colors.white,
                  boxShadow: const [
                    BoxShadow(
                      color: Color(0x25413368),
                      offset: Offset(0, 3),
                      blurRadius: 6,
                    )
                  ],
                  borderRadius: BorderRadius.circular(20)),
              child: Column(
                children: [
                  InkWell(
                    onTap: () {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (BuildContext context) =>
                              const ImacDetail()));
                    },
                    child: Container(
                      margin: const EdgeInsets.all(10),
                      child: Image.asset(
                        'assets/image/imac.png',
                        width: 100,
                        height: 100,
                      ),
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.only(bottom: 8),
                    alignment: Alignment.centerLeft,
                    child: const Text(
                      'Imac',
                      style: TextStyle(
                          fontSize: 20,
                          color: Color(0xFF4C53A5),
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  Container(
                    alignment: Alignment.centerLeft,
                    child: const Text(
                      'IMAC 21 Inch 2015 Core I7...',
                      style: TextStyle(
                        fontSize: 16,
                      ),
                    ),
                  ),
                  const Padding(
                    padding: EdgeInsets.symmetric(vertical: 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          'Rp. 6.000.000',
                          style: TextStyle(
                              fontSize: 15, fontWeight: FontWeight.bold),
                        ),
                        Icon(
                          Icons.shopping_cart_checkout,
                          size: 17,
                          color: Color(0xFF4C53A5),
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
            Container(
              padding: const EdgeInsets.only(left: 15, right: 15),
              margin: const EdgeInsets.symmetric(vertical: 8, horizontal: 10),
              decoration: BoxDecoration(
                  color: Colors.white,
                  boxShadow: const [
                    BoxShadow(
                      color: Color(0x25413368),
                      offset: Offset(0, 3),
                      blurRadius: 6,
                    )
                  ],
                  borderRadius: BorderRadius.circular(20)),
              child: Column(
                children: [
                  InkWell(
                    onTap: () {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (BuildContext context) =>
                              const KeyboardDetail()));
                    },
                    child: Container(
                      margin: const EdgeInsets.all(10),
                      child: Image.asset(
                        'assets/image/keyboard_rexus.png',
                        width: 130,
                        height: 100,
                      ),
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.only(bottom: 8),
                    alignment: Alignment.centerLeft,
                    child: const Text(
                      'Keyboard',
                      style: TextStyle(
                          fontSize: 20,
                          color: Color(0xFF4C53A5),
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  Container(
                    alignment: Alignment.centerLeft,
                    child: const Text(
                      'Rexus Keyboard Gaming Mini Battle...',
                      style: TextStyle(
                        fontSize: 15,
                      ),
                    ),
                  ),
                  const Padding(
                    padding: EdgeInsets.symmetric(vertical: 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          'Rp. 210.000',
                          style: TextStyle(
                              fontSize: 15, fontWeight: FontWeight.bold),
                        ),
                        Icon(
                          Icons.shopping_cart_checkout,
                          size: 17,
                          color: Color(0xFF4C53A5),
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
            Container(
              padding: const EdgeInsets.only(left: 15, right: 15),
              margin: const EdgeInsets.symmetric(vertical: 8, horizontal: 10),
              decoration: BoxDecoration(
                  color: Colors.white,
                  boxShadow: const [
                    BoxShadow(
                      color: Color(0x25413368),
                      offset: Offset(0, 3),
                      blurRadius: 6,
                    )
                  ],
                  borderRadius: BorderRadius.circular(20)),
              child: Column(
                children: [
                  InkWell(
                    onTap: () {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (BuildContext context) =>
                              const IpadDetail()));
                    },
                    child: Container(
                      margin: const EdgeInsets.all(10),
                      child: Image.asset(
                        'assets/image/ipad.png',
                        width: 120,
                        height: 100,
                      ),
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.only(bottom: 8),
                    alignment: Alignment.centerLeft,
                    child: const Text(
                      'Ipad',
                      style: TextStyle(
                          fontSize: 20,
                          color: Color(0xFF4C53A5),
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  Container(
                    alignment: Alignment.centerLeft,
                    child: const Text(
                      'APPLE iPAD PRO 11inch M1 2TB...',
                      style: TextStyle(
                        fontSize: 15,
                      ),
                    ),
                  ),
                  const Padding(
                    padding: EdgeInsets.symmetric(vertical: 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          'Rp. 16.900.000',
                          style: TextStyle(
                              fontSize: 15, fontWeight: FontWeight.bold),
                        ),
                        Icon(
                          Icons.shopping_cart_checkout,
                          size: 17,
                          color: Color(0xFF4C53A5),
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
            Container(
              padding: const EdgeInsets.only(left: 15, right: 15),
              margin: const EdgeInsets.symmetric(vertical: 8, horizontal: 10),
              decoration: BoxDecoration(
                  color: Colors.white,
                  boxShadow: const [
                    BoxShadow(
                      color: Color(0x25413368),
                      offset: Offset(0, 3),
                      blurRadius: 6,
                    )
                  ],
                  borderRadius: BorderRadius.circular(20)),
              child: Column(
                children: [
                  InkWell(
                    onTap: () {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (BuildContext context) =>
                              const ProyektorDetail()));
                    },
                    child: Container(
                      margin: const EdgeInsets.all(10),
                      child: Image.asset(
                        'assets/image/proyektor.png',
                        width: 120,
                        height: 100,
                      ),
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.only(bottom: 8),
                    alignment: Alignment.centerLeft,
                    child: const Text(
                      'Proyektor',
                      style: TextStyle(
                          fontSize: 20,
                          color: Color(0xFF4C53A5),
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  Container(
                    alignment: Alignment.centerLeft,
                    child: const Text(
                      'Proyektor EPSON EB-X500 EBX500 EB...',
                      style: TextStyle(
                        fontSize: 15,
                      ),
                    ),
                  ),
                  const Padding(
                    padding: EdgeInsets.symmetric(vertical: 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          'Rp. 5.300.000',
                          style: TextStyle(
                              fontSize: 15, fontWeight: FontWeight.bold),
                        ),
                        Icon(
                          Icons.shopping_cart_checkout,
                          size: 17,
                          color: Color(0xFF4C53A5),
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
            Container(
              padding: const EdgeInsets.only(left: 15, right: 15),
              margin: const EdgeInsets.symmetric(vertical: 8, horizontal: 10),
              decoration: BoxDecoration(
                  color: Colors.white,
                  boxShadow: const [
                    BoxShadow(
                      color: Color(0x25413368),
                      offset: Offset(0, 3),
                      blurRadius: 6,
                    )
                  ],
                  borderRadius: BorderRadius.circular(20)),
              child: Column(
                children: [
                  InkWell(
                    onTap: () {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (BuildContext context) =>
                              const MouseDetail()));
                    },
                    child: Container(
                      margin: const EdgeInsets.all(10),
                      child: Image.asset(
                        'assets/image/mouse.png',
                        width: 140,
                        height: 75,
                      ),
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.only(bottom: 8),
                    alignment: Alignment.centerLeft,
                    child: const Text(
                      'Mouse',
                      style: TextStyle(
                          fontSize: 20,
                          color: Color(0xFF4C53A5),
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  Container(
                    alignment: Alignment.centerLeft,
                    child: const Text(
                      'Mouse Gaming Rexus Shaga RX130...',
                      style: TextStyle(
                        fontSize: 15,
                      ),
                    ),
                  ),
                  const Padding(
                    padding: EdgeInsets.symmetric(vertical: 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          'Rp. 310.000',
                          style: TextStyle(
                              fontSize: 15, fontWeight: FontWeight.bold),
                        ),
                        Icon(
                          Icons.shopping_cart_checkout,
                          size: 17,
                          color: Color(0xFF4C53A5),
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
          ],
        ));
  }
}
