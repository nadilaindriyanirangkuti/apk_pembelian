import 'package:flutter/material.dart';
import 'package:pembelian_mobileapk/Moduls/http_provider.dart';
import 'package:pembelian_mobileapk/Screen/final_checkkout_screen.dart';
import 'package:provider/provider.dart';

class PaymentPage extends StatelessWidget {
  static const routeName = '/payment';
  const PaymentPage({super.key});

  @override
  Widget build(BuildContext context) {
    final httpProvider = Provider.of<HttpProvider>(context, listen: false);
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Image.asset(
          'assets/image/appbar.png',
          width: 130,
          fit: BoxFit.fill,
        ),
      ),
      body: FutureBuilder(
        future: httpProvider.checkRekening(),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Center(child: CircularProgressIndicator());
          } else if (snapshot.hasError) {
            return Center(child: Text('Error: ${snapshot.error}'));
          } else {
            return Column(
              children: [
                Container(
                  padding: const EdgeInsets.all(25),
                  width: MediaQuery.of(context).size.width,
                  child: const Text(
                    'Payment',
                    style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold),
                  ),
                ),
                Consumer<HttpProvider>(
                  builder: (context, value, child) {
                    print('value.data: ${value.data}');
                    return ListView.builder(
                      shrinkWrap: true,
                      itemCount: value.data.length,
                      itemBuilder: (context, index) {
                        final item = value.data[index];
                        print('item: $item');
                        return Container(
                          width: MediaQuery.of(context).size.width,
                          height: 70,
                          padding: const EdgeInsets.only(left: 15, right: 15),
                          margin: const EdgeInsets.symmetric(
                              vertical: 8, horizontal: 10),
                          decoration: BoxDecoration(
                              color: item['isChecked']
                                  ? Colors.grey
                                  : const Color(0xffd9d9d9),
                              // color: Colors.grey,
                              boxShadow: const [
                                BoxShadow(
                                  blurRadius: 0,
                                )
                              ],
                              borderRadius: BorderRadius.circular(10)),
                          child: Row(
                            children: [
                              InkWell(
                                onTap: () {
                                  value.onTapCard(index);
                                },
                                child: Container(
                                  margin: const EdgeInsets.all(10),
                                  child: Image.asset(
                                    'assets/image/${item['nama_bank']}.png',
                                    width: 70,
                                    height: 100,
                                  ),
                                ),
                              ),
                              const SizedBox(
                                width: 100,
                              ),
                              Container(
                                child: Text(
                                  '${item['norek']}',
                                  style: TextStyle(fontSize: 18),
                                ),
                              )
                            ],
                          ),
                        );
                      },
                    );
                  },
                ),
              ],
            );
          }
        },
      ),
      bottomNavigationBar: Container(
        width: MediaQuery.of(context).size.width,
        height: 50,
        margin: const EdgeInsets.all(15),
        child: Consumer<HttpProvider>(
          builder: (context, value, child) {
            return ElevatedButton(
              style: ElevatedButton.styleFrom(
                  // backgroundColor: const Color(0xff2E2977),
                  backgroundColor: value.norekChecked.isNotEmpty
                      ? const Color(0xff2E2977)
                      : Colors.grey[300],
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10))),
              onPressed: () {
                if (value.norekChecked.isNotEmpty) {
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (BuildContext context) => CheckoutPage(
                        norek: value.norekChecked,
                        namaBank: value.namaBankChecked,
                      ),
                    ),
                  );
                }
              },
              child: Text(
                'Lanjutkan',
                style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                  color: value.norekChecked.isNotEmpty
                      ? Colors.white
                      : Color(0xFF6E7179),
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
