import 'package:flutter/material.dart';
import 'package:pembelian_mobileapk/Moduls/product_provider.dart';
import 'package:pembelian_mobileapk/Util/string_util.dart';
import 'package:provider/provider.dart';
import 'package:pembelian_mobileapk/Screen/final_checkkout_screen.dart';
import 'package:pembelian_mobileapk/Screen/payment_screen.dart';

import 'package:pembelian_mobileapk/Widget/typography_widget.dart';

class CheckoutView extends StatelessWidget {
  static const routeName = '/checkout';
  final String product;
  final String shortName;
  final int harga;
  final int jumlah;
  final String image;
  const CheckoutView({
    super.key,
    this.product =
        'Apple MacBook Air (13.6 inci, M2, 2022) 8C CPU, 8C GPU, 256GB, Midnight',
    this.shortName = 'MacBook',
    this.harga = 14000000,
    this.jumlah = 1,
    this.image = 'macbook.png',
  });

  @override
  Widget build(BuildContext context) {
    final productProvider =
        Provider.of<ProductProvider>(context, listen: false);
    productProvider.setProduct(product, shortName, harga, jumlah, image);
    return Scaffold(
      appBar: AppBar(
        scrolledUnderElevation: 0,
        centerTitle: true,
        elevation: 0,
        title: Image.asset(
          'assets/image/appbar.png',
          width: 130,
          fit: BoxFit.fill,
          // height: 170,
        ),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            padding: const EdgeInsets.all(20),
            child: Row(
              children: [
                ClipRRect(
                  borderRadius: BorderRadius.circular(6.0),
                  child: Image.asset(
                    'assets/image/${productProvider.product['image']}',
                    fit: BoxFit.cover,
                    width: 150,
                    height: 150,
                  ),
                ),
                const SizedBox(
                  width: 16,
                ),
                Expanded(
                    child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      productProvider.product['nama'],
                      style: const TextStyle(
                          fontSize: 18, fontWeight: FontWeight.w600),
                    ),
                    const SizedBox(height: 8),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          (productProvider.product['harga'] as int).currency,
                          style: const TextStyle(
                            fontSize: 17,
                            fontWeight: FontWeight.bold,
                            color: Color(0xFFFF6B00),
                          ),
                        ),
                        Text(
                          'x${productProvider.product['jumlah']}',
                          style: const TextStyle(
                            fontSize: 17,
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(height: 12),
                  ],
                ))
              ],
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          Container(
            width: MediaQuery.of(context).size.width,
            margin: const EdgeInsets.only(left: 20),
            child: const Text(
              'Ringkasan Pembayaran',
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.w700),
            ),
          ),
          const Divider(
            indent: 20,
            endIndent: 20,
          ),
          const SizedBox(
            height: 30,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const Expanded(
                  child: Text(
                    'Total Harga',
                    style: TextStyle(fontSize: 20),
                  ),
                ),
                Text(
                  (productProvider.product['total_harga'] as int).currency,
                  style: const TextStyle(fontSize: 20),
                )
              ],
            ),
          ),
          const SizedBox(
            height: 5,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const Expanded(
                  child: Text(
                    'Ongkos Kirim',
                    style: TextStyle(fontSize: 20),
                  ),
                ),
                Text(
                  (productProvider.product['ongkir'] as int).currency,
                  style: const TextStyle(fontSize: 20),
                )
              ],
            ),
          ),
          const SizedBox(
            height: 70,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const Expanded(
                  child: Text(
                    'Total Pembayaran',
                    style: TextStyle(fontSize: 20),
                  ),
                ),
                Text(
                  (productProvider.product['total_bayar'] as int).currency,
                  style: const TextStyle(fontSize: 20),
                )
              ],
            ),
          ),
          const SizedBox(height: 30),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: InkWell(
              onTap: () {
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (BuildContext context) => const PaymentPage(),
                  ),
                );
              },
              child: Card(
                color: const Color(0xFFF2F2F2),
                child: Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 16, vertical: 12),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      const Text(
                        'Metode Pembayaran',
                        style: TextStyle(
                          color: Color(0xFF6E7179),
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      ClipRRect(
                        borderRadius: BorderRadius.circular(
                            30.0), // Radius untuk membuat rounded
                        child: Container(
                          height: 32,
                          width: 32,
                          padding: const EdgeInsets.all(0),
                          color: const Color(0xffDED6F1), // Warna background
                          child: IconButton(
                            padding: const EdgeInsets.all(0),
                            onPressed: () {},
                            icon: const Icon(Icons.arrow_forward),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
      bottomNavigationBar: Container(
        width: 160,
        height: 50,
        margin: const EdgeInsets.only(left: 200, bottom: 10, right: 10),
        child: ElevatedButton(
            style: ElevatedButton.styleFrom(
              backgroundColor: Colors.grey[300],
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10),
              ),
            ),
            onPressed: () {},
            child: const Text(
              'Bayar Sekarang',
              style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.bold,
                color: Color(0xFF6E7179),
              ),
            )),
      ),
    );
  }
}
