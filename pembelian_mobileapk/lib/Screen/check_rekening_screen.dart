import 'package:flutter/material.dart';
import 'package:pembelian_mobileapk/Moduls/http_provider.dart';
import 'package:pembelian_mobileapk/Moduls/product_provider.dart';
import 'package:provider/provider.dart';

class CheckRekeningScreen extends StatelessWidget {
  final String norekTujuan;
  CheckRekeningScreen({super.key, this.norekTujuan = '10016123456'});

  @override
  Widget build(BuildContext context) {
    final httpProvider = Provider.of<HttpProvider>(context, listen: false);
    final productProvider =
        Provider.of<ProductProvider>(context, listen: false);

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Image.asset(
          'assets/image/appbar.png',
          width: 130,
          fit: BoxFit.fill,
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 80),
        child: Form(
          key: httpProvider.formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Center(
                child: Text(
                  'Validasi Rekening',
                  style: TextStyle(fontSize: 24, fontWeight: FontWeight.w700),
                ),
              ),
              const SizedBox(height: 28),
              Text(
                'Nomor Rekening',
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.w600),
              ),
              const SizedBox(height: 4),
              TextFormField(
                // Assign the controller
                controller: httpProvider.norekController,
                style: const TextStyle(color: Colors.black, fontSize: 15.0),
                onChanged: (value) {},
                decoration: InputDecoration(
                  contentPadding: const EdgeInsets.symmetric(
                    vertical: 10,
                    horizontal: 16,
                  ),
                  // contentPadding: const EdgeInsets.only(top: 30, bottom: 0),
                  hintText: 'Masukkan nomor rekening',
                  hintStyle: TextStyle(
                      fontSize: 17,
                      fontWeight: FontWeight.normal,
                      color: Colors.grey),
                  border: const OutlineInputBorder(),
                  enabledBorder: const OutlineInputBorder(
                    borderSide: BorderSide(
                      color: const Color(0xFF2E2977),
                      width: 1,
                    ),
                  ),
                ),
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Silakan masukkan Norek Anda';
                  }
                  return null;
                },
              ),
              const SizedBox(height: 12),
              Text(
                'PIN',
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.w600),
              ),
              const SizedBox(height: 4),
              TextFormField(
                // Assign the controller
                controller: httpProvider.pinController,
                style: const TextStyle(color: Colors.black, fontSize: 15.0),
                onChanged: (value) {},
                decoration: InputDecoration(
                  contentPadding: const EdgeInsets.symmetric(
                    vertical: 10,
                    horizontal: 16,
                  ),
                  // contentPadding: const EdgeInsets.only(top: 30, bottom: 0),
                  hintText: 'Masukkan PIN',
                  hintStyle: TextStyle(
                      fontSize: 17,
                      fontWeight: FontWeight.normal,
                      color: Colors.grey),
                  border: const OutlineInputBorder(),
                  enabledBorder: const OutlineInputBorder(
                    borderSide: BorderSide(
                      color: const Color(0xFF2E2977),
                      width: 1,
                    ),
                  ),
                ),
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Silakan masukkan PIN Anda';
                  }
                  return null;
                },
              ),
            ],
          ),
        ),
      ),
      bottomNavigationBar: Container(
        margin: EdgeInsets.all(20),
        child: ElevatedButton(
          onPressed: () {
            Map<String, dynamic> payload = {
              'norek': httpProvider.norekController.text,
              'pin': httpProvider.pinController.text,
              'norek_tujuan': norekTujuan,
              'nominal': (productProvider.product['total_bayar'] as int),
              'keterangan': '${productProvider.product['short_name']}',
              'short_name': productProvider.product['short_name'],
              'harga': productProvider.product['harga'],
              'ongkir': productProvider.product['ongkir'],
              'total_harga': productProvider.product['total_harga'],
              'total_bayar': productProvider.product['total_bayar'],
              'jumlah': productProvider.product['jumlah'],
            };

            httpProvider.submitForm(context, payload);
          },
          child: Text(
            'Validasi',
            style: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.bold,
              color: const Color.fromARGB(255, 250, 239, 239),
            ),
          ),
          style: ElevatedButton.styleFrom(
            backgroundColor: const Color(0xff2E2977),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10),
            ),
          ),
        ),
      ),
    );
  }
}
