import 'package:flutter/material.dart';
import 'package:pembelian_mobileapk/Moduls/check_saldo_provider.dart';
import 'package:pembelian_mobileapk/Screen/home_screen.dart';
import 'package:pembelian_mobileapk/Screen/top_up_saldo_screen.dart';
import 'package:pembelian_mobileapk/Util/string_util.dart';
import 'package:pembelian_mobileapk/Widget/form_widget.dart';
import 'package:pembelian_mobileapk/Widget/typography_widget.dart';
import 'package:provider/provider.dart';

class CheckSaldoScreen extends StatelessWidget {
  const CheckSaldoScreen({super.key});

  @override
  Widget build(BuildContext context) {
    final checkSaldoProvider =
        Provider.of<CheckSaldoProvider>(context, listen: false);
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Image.asset(
          'assets/image/appbar.png',
          width: 130,
          fit: BoxFit.fill,
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 40),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Form(
              key: checkSaldoProvider.formKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  TextFormFieldBorder(
                    label: 'Nomor Rekening',
                    textController: checkSaldoProvider.norekController,
                    hintText: 'Masukkan nomor rekening',
                    messageValidator: 'Silakan masukkan NOREK Anda',
                  ),
                  const SizedBox(height: 28),
                  Row(
                    children: [
                      Expanded(
                        child: ElevatedButton(
                          onPressed: () {
                            Map<String, dynamic> payload = {
                              'norek': checkSaldoProvider.norekController.text,
                            };
                            checkSaldoProvider.submitForm(context, payload);
                          },
                          style: ElevatedButton.styleFrom(
                            backgroundColor: const Color(0xff2E2977),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10),
                            ),
                          ),
                          child: const Text(
                            'Cek Saldo',
                            style: TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.bold,
                              color: Color(0xFFFAEFEF),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            const SizedBox(height: 60),
            Consumer<CheckSaldoProvider>(
              builder: (context, value, child) {
                if (value.data.isEmpty) {
                  return const SizedBox();
                } else {
                  return Container(
                    padding: const EdgeInsets.all(15),
                    decoration: BoxDecoration(
                      border: Border.all(
                        color: const Color(0xFF2E2977), // Warna border
                        width: 1, // Ketebalan border
                      ),
                      borderRadius: BorderRadius.circular(
                          12.0), // Border radius untuk sudut melengkung
                    ),
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Expanded(
                              child: Text(
                                'Nomor Rekening',
                                style: XTypo.font18Normal(),
                              ),
                            ),
                            Expanded(
                              child: Text(
                                ': ${value.data['norek']}',
                                style: XTypo.font18Bold(),
                              ),
                            ),
                          ],
                        ),
                        const SizedBox(height: 12),
                        Row(
                          children: [
                            Expanded(
                              child: Text(
                                'Nama',
                                style: XTypo.font18Normal(),
                              ),
                            ),
                            Expanded(
                              child: Text(
                                ': ${value.data['nama']}',
                                overflow: TextOverflow.ellipsis,
                                style: XTypo.font18Bold(),
                              ),
                            ),
                          ],
                        ),
                        const SizedBox(height: 12),
                        Row(
                          children: [
                            Expanded(
                              child: Text(
                                'Nama Bank',
                                style: XTypo.font18Normal(),
                              ),
                            ),
                            Expanded(
                              child: Text(
                                ': ${value.data['nama_bank']}',
                                style: XTypo.font18Bold(),
                              ),
                            ),
                          ],
                        ),
                        const SizedBox(height: 12),
                        Row(
                          children: [
                            Expanded(
                              child: Text(
                                'Saldo',
                                style: XTypo.font18Normal(),
                              ),
                            ),
                            Expanded(
                              child: Text(
                                ': ${((value.data['saldo'] as double).toInt()).currency}',
                                style: XTypo.font18Bold(),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  );
                }
              },
            ),
          ],
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: Colors.white,
        currentIndex: 1,
        onTap: (index) {
          // Mengubah halaman yang aktif saat item bottom navigation diubah
          if (index == 0) {
            Navigator.of(context).push(
              MaterialPageRoute(
                builder: (BuildContext context) => HomeScreen(),
              ),
            );
          } else if (index == 2) {
            Navigator.of(context).push(
              MaterialPageRoute(
                builder: (BuildContext context) => TopUpSaldoScreen(),
              ),
            );
          }
        },
        items: const [
          BottomNavigationBarItem(
            icon: Icon(Icons.production_quantity_limits),
            label: 'Product',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.monetization_on),
            label: 'Cek Saldo',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.wallet),
            label: 'Top Up',
          ),
        ],
      ),
    );
  }
}
