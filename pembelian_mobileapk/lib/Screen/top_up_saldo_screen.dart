import 'package:flutter/material.dart';
import 'package:pembelian_mobileapk/Moduls/top_up_saldo_provider.dart';
import 'package:pembelian_mobileapk/Screen/check_saldo_screen.dart';
import 'package:pembelian_mobileapk/Screen/dialog_screen.dart';
import 'package:pembelian_mobileapk/Screen/home_screen.dart';
import 'package:pembelian_mobileapk/Widget/dialog_success_top_up_widget.dart';
import 'package:pembelian_mobileapk/Widget/form_widget.dart';
import 'package:pembelian_mobileapk/Widget/typography_widget.dart';
import 'package:provider/provider.dart';

class TopUpSaldoScreen extends StatelessWidget {
  const TopUpSaldoScreen({super.key});

  @override
  Widget build(BuildContext context) {
    final topUpSaldoProvider =
        Provider.of<TopUpSaldoProvider>(context, listen: false);
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Image.asset(
          'assets/image/appbar.png',
          width: 130,
          fit: BoxFit.fill,
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 60),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            Form(
              key: topUpSaldoProvider.formKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Center(
                    child: Text(
                      'Top Up Saldo',
                      style:
                          TextStyle(fontSize: 24, fontWeight: FontWeight.w700),
                    ),
                  ),
                  const SizedBox(height: 28),
                  TextFormFieldBorder(
                    label: 'Nomor Rekening',
                    textController: topUpSaldoProvider.norekController,
                    hintText: 'Masukkan nomor rekening',
                    messageValidator: 'Silakan masukkan Norek Anda',
                  ),
                  const SizedBox(height: 12),
                  TextFormFieldBorder(
                    label: 'PIN',
                    textController: topUpSaldoProvider.pinController,
                    hintText: 'Masukkan PIN',
                    messageValidator: 'Silakan masukkan PIN Anda',
                  ),
                  const SizedBox(height: 12),
                  TextFormFieldBorder(
                    label: 'Nominal',
                    textController: topUpSaldoProvider.nominalController,
                    hintText: 'Masukkan nominal',
                    messageValidator: 'Silakan masukkan Nominal',
                  ),
                ],
              ),
            ),
            const SizedBox(height: 28),
            Row(
              children: [
                Expanded(
                  child: ElevatedButton(
                    onPressed: () {
                      Map<String, dynamic> payload = {
                        'norek': topUpSaldoProvider.norekController.text,
                        'pin': topUpSaldoProvider.pinController.text,
                        'nominal': int.parse(
                          topUpSaldoProvider.nominalController.text,
                        ),
                      };
                      topUpSaldoProvider.submitForm(context, payload);
                    },
                    style: ElevatedButton.styleFrom(
                      backgroundColor: const Color(0xff2E2977),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10),
                      ),
                    ),
                    child: const Text(
                      'Top Up',
                      style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                        color: Color(0xFFFAEFEF),
                      ),
                    ),
                  ),
                ),
              ],
            )
          ],
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: Colors.white,
        currentIndex: 2,
        onTap: (index) {
          // Mengubah halaman yang aktif saat item bottom navigation diubah
          if (index == 0) {
            Navigator.of(context).push(
              MaterialPageRoute(
                builder: (BuildContext context) => HomeScreen(),
              ),
            );
          } else if (index == 1) {
            Navigator.of(context).push(
              MaterialPageRoute(
                builder: (BuildContext context) => CheckSaldoScreen(),
              ),
            );
          }
        },
        items: const [
          BottomNavigationBarItem(
            icon: Icon(Icons.production_quantity_limits),
            label: 'Product',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.monetization_on),
            label: 'Cek Saldo',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.wallet),
            label: 'Top Up',
          ),
        ],
      ),
    );
  }
}
