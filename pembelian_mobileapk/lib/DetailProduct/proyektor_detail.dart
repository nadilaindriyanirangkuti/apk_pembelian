import 'package:flutter/material.dart';
import 'package:pembelian_mobileapk/Screen/checkout_screen.dart';
import 'package:pembelian_mobileapk/Widget/typography_widget.dart';

class ProyektorDetail extends StatefulWidget {
  const ProyektorDetail({super.key});

  @override
  State<ProyektorDetail> createState() => _ProyektorDetailState();
}

class _ProyektorDetailState extends State<ProyektorDetail> {
  int _counter = 1;

  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }

  void _decrementCounter() {
    setState(() {
      if (_counter > 0) {
        _counter--;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
          elevation: 0,
          surfaceTintColor: Colors.white,
          forceMaterialTransparency: true,
          scrolledUnderElevation: 0.0,
          centerTitle: true,
          title: Image.asset(
            'assets/image/appbar.png',
            width: 130,
            fit: BoxFit.fill,
          )),
      body: ListView(
        children: [
          Padding(
            padding: const EdgeInsets.all(16),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(20),
              child: SizedBox.fromSize(
                size: const Size.fromRadius(100),
                child: Image.asset("assets/image/proyektor.png",
                    fit: BoxFit.contain),
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(left: 20),
            // padding: EdgeInsets.all(10),
            child: Text(
              "Proyektor EPSON EB-X500 EBX500 EB X500 Pengganti EB X400 EBX400 EB-X400 XGA 3600",
              style: TextStyle(
                fontSize: 18,
                color: Color(0xFF4C53A5),
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(left: 20, right: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "Rp. 5.300.000",
                  style: TextStyle(
                    fontSize: 16,
                    color: Color(0xffFF6B00),
                    fontWeight: FontWeight.normal,
                  ),
                ),
                Container(
                  width: 110,
                  decoration: BoxDecoration(
                    color: Color(0xFFDED6F1),
                    borderRadius: BorderRadius.circular(25),
                  ),
                  child: Row(
                    children: [
                      IconButton(
                        constraints: BoxConstraints(minWidth: 0, minHeight: 0),
                        onPressed: _decrementCounter,
                        icon: Icon(
                          Icons.remove,
                        ),
                        iconSize: 16,
                      ),
                      Text(
                        '$_counter',
                        style: XTypo.font16W500(),
                      ),
                      IconButton(
                        constraints: BoxConstraints(minWidth: 0, minHeight: 0),
                        onPressed: _incrementCounter,
                        icon: Icon(
                          Icons.add,
                        ),
                        iconSize: 16,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Container(
            margin: EdgeInsets.only(left: 20),
            child: Text(
              "Deskripsi",
              style: TextStyle(
                fontSize: 20,
                color: Color(0xFF4C53A5),
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          SizedBox(
            height: 12,
          ),
          // Container(
          //   margin: EdgeInsets.only(left: 20),
          //   child: Text(
          //     "Spesifikasi Produk",
          //     textAlign: TextAlign.start,
          //     style: TextStyle(
          //         fontSize: 17,
          //         color: Colors.black,
          //         fontWeight: FontWeight.bold),
          //   ),
          // ),
          Container(
            margin: EdgeInsets.only(left: 20),
            child: Text(
              "EPSON EB-X500",
              textAlign: TextAlign.start,
              style: TextStyle(
                fontSize: 15,
                color: Colors.black,
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(left: 20),
            child: Text(
              "XGA, 3600 Ansi Lumens",
              textAlign: TextAlign.start,
              style: TextStyle(
                fontSize: 15,
                color: Colors.black,
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(left: 20),
            child: Text(
              "3 LCD Technology",
              textAlign: TextAlign.start,
              style: TextStyle(
                fontSize: 15,
                color: Colors.black,
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(left: 20),
            child: Text(
              "Contrast Ratio 16.000:1",
              textAlign: TextAlign.start,
              style: TextStyle(
                fontSize: 15,
                color: Colors.black,
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(left: 20),
            child: Text(
              "HDMI, RGB-IN, RCA, USB Type A/B",
              textAlign: TextAlign.start,
              style: TextStyle(
                fontSize: 15,
                color: Colors.black,
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(left: 20),
            child: Text(
              "Auto V-Keystone , H-Keystone slider",
              textAlign: TextAlign.start,
              style: TextStyle(
                fontSize: 15,
                color: Colors.black,
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(left: 20),
            child: Text(
              "Optional Wi-FI Dual Brand , ELPAP 11(2.4/5 GHZ)",
              textAlign: TextAlign.start,
              style: TextStyle(
                fontSize: 15,
                color: Colors.black,
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(left: 20),
            child: Text(
              "AUTO POWER ON",
              textAlign: TextAlign.start,
              style: TextStyle(
                fontSize: 15,
                color: Colors.black,
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(left: 20),
            child: Text(
              "AUTO SEARCH SOURCE",
              textAlign: TextAlign.start,
              style: TextStyle(
                fontSize: 15,
                color: Colors.black,
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(left: 20),
            child: Text(
              "MULTI PC-PROJECTION",
              textAlign: TextAlign.start,
              style: TextStyle(
                fontSize: 15,
                color: Colors.black,
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(left: 20),
            child: Text(
              "LAMP LIFE UP TO 12.000 HOURS (ECO MODE)",
              textAlign: TextAlign.start,
              style: TextStyle(
                fontSize: 15,
                color: Colors.black,
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(left: 20),
            child: Text(
              "Garansi Resmi Nasional =  3 Tahun Parts dan Service ",
              textAlign: TextAlign.start,
              style: TextStyle(
                fontSize: 15,
                color: Colors.black,
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(left: 20),
            child: Text(
              "Service lampu = 2 Tahun atau 1.000 jam",
              textAlign: TextAlign.start,
              style: TextStyle(
                fontSize: 15,
                color: Colors.black,
              ),
            ),
          ),
        ],
      ),
      bottomNavigationBar: Container(
        width: 160,
        height: 50,
        margin: const EdgeInsets.only(left: 250, bottom: 10, right: 10),
        child: ElevatedButton(
            style: ElevatedButton.styleFrom(
                backgroundColor: const Color(0xff2E2977),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10))),
            onPressed: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (BuildContext context) => CheckoutView(
                    product:
                        'Proyektor EPSON EB-X500 EBX500 EB X500 Pengganti EB X400 EBX400 EB-X400 XGA 3600',
                    shortName: 'Proyektor',
                    harga: 5300000,
                    jumlah: _counter,
                    image: 'proyektor.png',
                  ),
                ),
              );
            },
            child: const Text(
              'Beli',
              style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.bold,
                color: Colors.white,
              ),
            )),
      ),
    );
  }
}
