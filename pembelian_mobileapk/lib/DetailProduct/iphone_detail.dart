import 'package:flutter/material.dart';
import 'package:pembelian_mobileapk/Screen/checkout_screen.dart';
import 'package:pembelian_mobileapk/Widget/typography_widget.dart';

class IphoneDetail extends StatefulWidget {
  const IphoneDetail({super.key});

  @override
  State<IphoneDetail> createState() => _IphoneDetailState();
}

class _IphoneDetailState extends State<IphoneDetail> {
  int _counter = 1;

  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }

  void _decrementCounter() {
    setState(() {
      if (_counter > 0) {
        _counter--;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: Colors.white,
          scrolledUnderElevation: 0,
          centerTitle: true,
          title: Image.asset(
            'assets/image/appbar.png',
            width: 130,
            fit: BoxFit.fill,
          ),
        ),
        body: ListView(children: [
          Padding(
            padding: const EdgeInsets.all(16),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(20),
              child: SizedBox.fromSize(
                size: const Size.fromRadius(100),
                child: Image.asset("assets/image/iphone.png"),
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(left: 20),
            // padding: EdgeInsets.all(10),
            child: Text(
              "iphone 128GB 13 RESMI PINK/BLUE/WHITE/BLACK",
              style: TextStyle(
                fontSize: 18,
                color: Color(0xFF4C53A5),
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          SizedBox(
            height: 15,
          ),
          Container(
            margin: EdgeInsets.only(left: 20, right: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "Rp. 17.000.000",
                  style: TextStyle(
                    fontSize: 16,
                    color: Color(0xffFF6B00),
                    fontWeight: FontWeight.normal,
                  ),
                ),
                Container(
                  width: 110,
                  decoration: BoxDecoration(
                    color: Color(0xFFDED6F1),
                    borderRadius: BorderRadius.circular(25),
                  ),
                  child: Row(
                    children: [
                      IconButton(
                        constraints: BoxConstraints(minWidth: 0, minHeight: 0),
                        onPressed: _decrementCounter,
                        icon: Icon(
                          Icons.remove,
                        ),
                        iconSize: 16,
                      ),
                      Text(
                        '$_counter',
                        style: XTypo.font16W500(),
                      ),
                      IconButton(
                        constraints: BoxConstraints(minWidth: 0, minHeight: 0),
                        onPressed: _incrementCounter,
                        icon: Icon(
                          Icons.add,
                        ),
                        iconSize: 16,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Container(
            margin: EdgeInsets.only(left: 20),
            child: Text(
              "Deskripsi",
              style: TextStyle(
                fontSize: 20,
                color: Color(0xFF4C53A5),
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          SizedBox(
            height: 12,
          ),
          Container(
            margin: EdgeInsets.only(left: 20),
            child: Text(
              "APPLE IPHONE 13 128GB RESMI IBOX",
              textAlign: TextAlign.start,
              style: TextStyle(
                fontSize: 15,
                color: Colors.black,
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(left: 20),
            child: Text(
              "WARNA TERSEDIA PINK/BLUE/WHITE/BLACK",
              textAlign: TextAlign.start,
              style: TextStyle(
                fontSize: 15,
                color: Colors.black,
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(left: 20),
            child: Text(
              "Isi box : Hanya unit dan kabel Type C to lighting (Adaptor dan Earphone belum include)",
              textAlign: TextAlign.start,
              style: TextStyle(
                fontSize: 15,
                color: Colors.black,
              ),
            ),
          ),
          SizedBox(
            height: 15,
          ),
          Container(
            margin: EdgeInsets.only(left: 20),
            child: Text(
              "Garansi Resmi Apple Indonesia 1 Tahun.",
              textAlign: TextAlign.start,
              style: TextStyle(
                fontSize: 15,
                color: Colors.black,
              ),
            ),
          ),
          SizedBox(
            height: 15,
          ),
          Container(
            margin: EdgeInsets.only(left: 20),
            child: Text(
              "Unitnya sama seperti yang Dijual Erafone, iBox, Hello Store, Digimap , Digiplus, Urban Republic dan Store lainnya. Segel Green Peel, Support All Operator Indonesia.",
              textAlign: TextAlign.start,
              style: TextStyle(
                fontSize: 15,
                color: Colors.black,
              ),
            ),
          ),
          SizedBox(
            height: 15,
          ),
          Container(
            margin: EdgeInsets.only(left: 20),
            child: Text(
              "TIDAK TERIMA KOMPLAIN TANPA VIDEO UNBOXING FULL Tanpa Jeda /EDIT [IKUTI KERTAS PANDUAN CEK FISIK]",
              textAlign: TextAlign.start,
              style: TextStyle(
                fontSize: 15,
                color: Colors.black,
              ),
            ),
          ),
          SizedBox(
            height: 15,
          ),
          Container(
            margin: EdgeInsets.only(left: 20),
            child: Text(
              "SK Retur barang / Garansi toko :",
              textAlign: TextAlign.start,
              style: TextStyle(
                fontSize: 15,
                color: Colors.black,
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(left: 20),
            child: Text(
              "*wajib sertakan video unboxing",
              textAlign: TextAlign.start,
              style: TextStyle(
                fontSize: 15,
                color: Colors.black,
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(left: 20),
            child: Text(
              "*unit belum aktifasi (connect ke wifi)",
              textAlign: TextAlign.start,
              style: TextStyle(
                fontSize: 15,
                color: Colors.black,
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(left: 20),
            child: Text(
              "Jika Unit Sudah Connect wifi , Kami Hanya Bisa bantu Claim ke Servis center Apple",
              textAlign: TextAlign.start,
              style: TextStyle(
                fontSize: 15,
                color: Colors.black,
              ),
            ),
          ),
        ]),
        bottomNavigationBar: Container(
          color: Colors.white,
          width: 160,
          height: 50,
          margin: const EdgeInsets.only(left: 250, bottom: 10, right: 10),
          child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                  backgroundColor: const Color(0xff2E2977),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10))),
              onPressed: () {
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (BuildContext context) => CheckoutView(
                      product: 'iphone 128GB 13 RESMI PINK/BLUE/WHITE/BLACK',
                      shortName: 'Iphone',
                      harga: 17000000,
                      jumlah: _counter,
                      image: 'iphone.png',
                    ),
                  ),
                );
              },
              child: const Text(
                'Beli',
                style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.bold,
                    color: Colors.white),
              )),
        ));
  }
}
