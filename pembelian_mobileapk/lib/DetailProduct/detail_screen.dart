import 'package:flutter/material.dart';
import 'package:pembelian_mobileapk/Moduls/product_provider.dart';
import 'package:pembelian_mobileapk/Screen/checkout_screen.dart';
import 'package:pembelian_mobileapk/Screen/final_checkkout_screen.dart';
import 'package:pembelian_mobileapk/Widget/typography_widget.dart';
import 'package:provider/provider.dart';

class DetailPageScreen extends StatefulWidget {
  const DetailPageScreen({super.key});

  @override
  State<DetailPageScreen> createState() => _DetailPageScreenState();
}

class _DetailPageScreenState extends State<DetailPageScreen> {
  int _counter = 1;

  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }

  void _decrementCounter() {
    setState(() {
      if (_counter > 0) {
        _counter--;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: Colors.white,
          scrolledUnderElevation: 0,
          centerTitle: true,
          title: Image.asset(
            'assets/image/appbar.png',
            width: 130,
            fit: BoxFit.fill,
          ),
        ),
        body: ListView(
          children: [
            Padding(
              padding: const EdgeInsets.all(16),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(20),
                child: SizedBox.fromSize(
                  size: const Size.fromRadius(100),
                  child: Image.asset("assets/image/macbook.png",
                      fit: BoxFit.contain),
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(left: 20),
              // padding: EdgeInsets.all(10),
              child: Text(
                "Apple MacBook Air (13.6 inci, M2, 2022) 8C CPU, 8C GPU, 256GB, Midnight",
                style: TextStyle(
                  fontSize: 20,
                  color: Color(0xFF4C53A5),
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(left: 20, right: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "Rp. 14.999.000",
                    style: TextStyle(
                      fontSize: 16,
                      color: Color(0xffFF6B00),
                      fontWeight: FontWeight.normal,
                    ),
                  ),
                  Container(
                    width: 110,
                    decoration: BoxDecoration(
                      color: Color(0xFFDED6F1),
                      borderRadius: BorderRadius.circular(25),
                    ),
                    child: Row(
                      children: [
                        IconButton(
                          constraints:
                              BoxConstraints(minWidth: 0, minHeight: 0),
                          onPressed: _decrementCounter,
                          icon: Icon(
                            Icons.remove,
                          ),
                          iconSize: 16,
                        ),
                        Text(
                          '$_counter',
                          style: XTypo.font16W500(),
                        ),
                        IconButton(
                          constraints:
                              BoxConstraints(minWidth: 0, minHeight: 0),
                          onPressed: _incrementCounter,
                          icon: Icon(
                            Icons.add,
                          ),
                          iconSize: 16,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Container(
              margin: EdgeInsets.only(left: 20),
              child: Text(
                "Deskripsi",
                style: TextStyle(
                  fontSize: 20,
                  color: Color(0xFF4C53A5),
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            SizedBox(
              height: 12,
            ),
            Container(
              margin: EdgeInsets.only(left: 20),
              child: Text(
                "Chip M2 dengan performa CPU, GPU, dan pembelajaran mesin generasi berikutnya CPU 8-core dan GPU hingga 10-core yang lebih cepat untuk menjalankan berbagai tugas kompleks Memori terintegrasi lebih cepat hingga 24 GB menjadikan segala yang Anda lakukan terasa lancar Penerapan filter dan efek gambar hingga 20 persen lebih cepat",
                textAlign: TextAlign.start,
                style: TextStyle(
                  fontSize: 15,
                  color: Colors.black,
                ),
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Container(
              margin: EdgeInsets.only(left: 20),
              child: Text(
                "Spesifikasi Produk",
                style: TextStyle(
                    fontSize: 17,
                    color: Colors.black,
                    fontWeight: FontWeight.bold),
              ),
            ),
            SizedBox(
              height: 5,
            ),
            Container(
              margin: EdgeInsets.only(left: 20),
              child: Text(
                "Merk : Apple",
                style: TextStyle(
                  fontSize: 15,
                  color: Colors.black,
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(left: 20),
              child: Text(
                "Ukuran Layar Laptop : 13",
                style: TextStyle(
                  fontSize: 15,
                  color: Colors.black,
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(left: 20),
              child: Text(
                "Kapasitas Penyimpanan : 256GB",
                style: TextStyle(
                  fontSize: 15,
                  color: Colors.black,
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(left: 20),
              child: Text(
                "Sistem Operasi : Mac",
                style: TextStyle(
                  fontSize: 15,
                  color: Colors.black,
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(left: 20),
              child: Text(
                "Masa Garansi : 12 Bulan",
                style: TextStyle(
                  fontSize: 15,
                  color: Colors.black,
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(left: 20),
              child: Text(
                "Jenis Garansi : Garansi Resmi",
                style: TextStyle(
                  fontSize: 15,
                  color: Colors.black,
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(left: 20),
              child: Text(
                "Model Laptop : Macbook Air M2 2022",
                style: TextStyle(
                  fontSize: 15,
                  color: Colors.black,
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(left: 20),
              child: Text(
                "Berat Produk : 2370g",
                style: TextStyle(
                  fontSize: 15,
                  color: Colors.black,
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(left: 20),
              child: Text(
                "Tipe Prosesor : Apple M2 Chip",
                style: TextStyle(
                  fontSize: 15,
                  color: Colors.black,
                ),
              ),
            ),
          ],
        ),
        bottomNavigationBar: Container(
          width: 160,
          height: 50,
          margin: const EdgeInsets.only(left: 250, bottom: 10, right: 10),
          child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                  backgroundColor: const Color(0xff2E2977),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10))),
              onPressed: () {
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (BuildContext context) => CheckoutView(
                      product:
                          'Apple MacBook Air (13.6 inci, M2, 2022) 8C CPU, 8C GPU, 256GB, Midnight',
                      shortName: 'Macbook',
                      harga: 14999000,
                      jumlah: _counter,
                      image: 'macbook.png',
                    ),
                  ),
                );
              },
              child: const Text(
                'Beli',
                style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.bold,
                    color: Colors.white),
              )),
        ));
  }
}
