import 'package:flutter/material.dart';
import 'package:pembelian_mobileapk/Screen/checkout_screen.dart';
import 'package:pembelian_mobileapk/Widget/typography_widget.dart';

class ImacDetail extends StatefulWidget {
  const ImacDetail({super.key});

  @override
  State<ImacDetail> createState() => _ImacDetailState();
}

class _ImacDetailState extends State<ImacDetail> {
  int _counter = 1;

  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }

  void _decrementCounter() {
    setState(() {
      if (_counter > 0) {
        _counter--;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
            elevation: 0,
            surfaceTintColor: Colors.white,
            forceMaterialTransparency: true,
            scrolledUnderElevation: 0.0,
            centerTitle: true,
            backgroundColor: Colors.white,
            title: Image.asset(
              'assets/image/appbar.png',
              width: 130,
              fit: BoxFit.fill,
            )),
        body: ListView(
          children: [
            Padding(
              padding: const EdgeInsets.all(16),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(20),
                child: SizedBox.fromSize(
                  size: const Size.fromRadius(100),
                  child:
                      Image.asset("assets/image/imac.png", fit: BoxFit.contain),
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(left: 20),
              // padding: EdgeInsets.all(10),
              child: Text(
                "IMAC RETINA 4K 21 INCH 2015 CORE I7 16/1TB ORIGINAL",
                style: TextStyle(
                  fontSize: 20,
                  color: Color(0xFF4C53A5),
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(left: 20, right: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "Rp. 6.000.000",
                    style: TextStyle(
                      fontSize: 16,
                      color: Color(0xffFF6B00),
                      fontWeight: FontWeight.normal,
                    ),
                  ),
                  Container(
                    width: 110,
                    decoration: BoxDecoration(
                      color: Color(0xFFDED6F1),
                      borderRadius: BorderRadius.circular(25),
                    ),
                    child: Row(
                      children: [
                        IconButton(
                          constraints:
                              BoxConstraints(minWidth: 0, minHeight: 0),
                          onPressed: _decrementCounter,
                          icon: Icon(
                            Icons.remove,
                          ),
                          iconSize: 16,
                        ),
                        Text(
                          '$_counter',
                          style: XTypo.font16W500(),
                        ),
                        IconButton(
                          constraints:
                              BoxConstraints(minWidth: 0, minHeight: 0),
                          onPressed: _incrementCounter,
                          icon: Icon(
                            Icons.add,
                          ),
                          iconSize: 16,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Container(
              margin: EdgeInsets.only(left: 20),
              child: Text(
                "Deskripsi",
                style: TextStyle(
                  fontSize: 20,
                  color: Color(0xFF4C53A5),
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            SizedBox(
              height: 12,
            ),
            Container(
              margin: EdgeInsets.only(left: 20),
              child: Text(
                "IMAC baru memiliki layar Retina 10,2 inci yang indah, chip A13 Bionic andal, kamera depan Ultra Wide dengan Center Stage, dan berfungsi dengan Apple Pencil dan Smart Keyboard.",
                textAlign: TextAlign.start,
                style: TextStyle(
                  fontSize: 15,
                  color: Colors.black,
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(left: 20),
              child: Text(
                "CORE : I7",
                textAlign: TextAlign.start,
                style: TextStyle(
                  fontSize: 15,
                  color: Colors.black,
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(left: 20),
              child: Text(
                "RAM  : 16 GB",
                textAlign: TextAlign.start,
                style: TextStyle(
                  fontSize: 15,
                  color: Colors.black,
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(left: 20),
              child: Text(
                "SSD   : 1 TB",
                textAlign: TextAlign.start,
                style: TextStyle(
                  fontSize: 15,
                  color: Colors.black,
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(left: 20),
              child: Text(
                "Kelengkapan Unit + Charger Kemulusan 90% sampai 98%",
                textAlign: TextAlign.start,
                style: TextStyle(
                  fontSize: 15,
                  color: Colors.black,
                ),
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Container(
              margin: EdgeInsets.only(left: 20),
              child: Text(
                "Garansi toko 2 bulan",
                textAlign: TextAlign.start,
                style: TextStyle(
                  fontSize: 15,
                  color: Colors.black,
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(left: 20),
              child: Text(
                "Garansi tukar unit 7 hari (jika barang yg datang tidak sesuai deskripsi)",
                textAlign: TextAlign.start,
                style: TextStyle(
                  fontSize: 15,
                  color: Colors.black,
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(left: 20),
              child: Text(
                "Original",
                textAlign: TextAlign.start,
                style: TextStyle(
                  fontSize: 15,
                  color: Colors.black,
                ),
              ),
            ),
          ],
        ),
        bottomNavigationBar: Container(
          width: 160,
          height: 50,
          margin: const EdgeInsets.only(left: 250, bottom: 10, right: 10),
          child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                  backgroundColor: const Color(0xff2E2977),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10))),
              onPressed: () {
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (BuildContext context) => CheckoutView(
                      product:
                          'IMAC RETINA 4K 21 INCH 2015 CORE I7 16/1TB ORIGINAL',
                      shortName: 'Imac',
                      harga: 6000000,
                      jumlah: _counter,
                      image: 'imac.png',
                    ),
                  ),
                );
              },
              child: const Text(
                'Beli',
                style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.bold,
                    color: Colors.white),
              )),
        ));
  }
}
