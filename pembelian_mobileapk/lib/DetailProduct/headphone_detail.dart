import 'package:flutter/material.dart';
import 'package:pembelian_mobileapk/Screen/checkout_screen.dart';
import 'package:pembelian_mobileapk/Widget/typography_widget.dart';

class HeadphoneDetail extends StatefulWidget {
  const HeadphoneDetail({super.key});

  @override
  State<HeadphoneDetail> createState() => _HeadphoneDetailState();
}

class _HeadphoneDetailState extends State<HeadphoneDetail> {
  int _counter = 1;

  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }

  void _decrementCounter() {
    setState(() {
      if (_counter > 0) {
        _counter--;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: 0,
          surfaceTintColor: Colors.white,
          forceMaterialTransparency: true,
          scrolledUnderElevation: 0.0,
          centerTitle: true,
          title: Image.asset(
            'assets/image/appbar.png',
            width: 130,
            fit: BoxFit.fill,
          ),
        ),
        body: ListView(children: [
          Padding(
            padding: const EdgeInsets.all(16),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(20),
              child: SizedBox.fromSize(
                size: const Size.fromRadius(100),
                child: Image.asset("assets/image/headphone.png"),
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(left: 20),
            // padding: EdgeInsets.all(10),
            child: Text(
              "Baseus Bowie H1 Noise-Collection Wireless/Wired Headphone ANC Audio",
              style: TextStyle(
                fontSize: 20,
                color: Color(0xFF4C53A5),
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          SizedBox(
            height: 15,
          ),
          Container(
            margin: EdgeInsets.only(left: 20, right: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "Rp. 500.000",
                  style: TextStyle(
                    fontSize: 16,
                    color: Color(0xffFF6B00),
                    fontWeight: FontWeight.normal,
                  ),
                ),
                Container(
                  width: 110,
                  decoration: BoxDecoration(
                    color: Color(0xFFDED6F1),
                    borderRadius: BorderRadius.circular(25),
                  ),
                  child: Row(
                    children: [
                      IconButton(
                        constraints: BoxConstraints(minWidth: 0, minHeight: 0),
                        onPressed: _decrementCounter,
                        icon: Icon(
                          Icons.remove,
                        ),
                        iconSize: 16,
                      ),
                      Text(
                        '$_counter',
                        style: XTypo.font16W500(),
                      ),
                      IconButton(
                        constraints: BoxConstraints(minWidth: 0, minHeight: 0),
                        onPressed: _incrementCounter,
                        icon: Icon(
                          Icons.add,
                        ),
                        iconSize: 16,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Container(
            margin: EdgeInsets.only(left: 20),
            child: Text(
              "Deskripsi",
              style: TextStyle(
                fontSize: 20,
                color: Color(0xFF4C53A5),
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          SizedBox(
            height: 12,
          ),
          Container(
            margin: EdgeInsets.only(left: 20),
            child: Text(
              "BS-EAR-H1i-BK = Garansi 1 Tahun",
              textAlign: TextAlign.start,
              style: TextStyle(
                fontSize: 15,
                color: Colors.black,
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(left: 20),
            child: Text(
              "BS-EAR-H1i-WH = Garansi 1 Tahun",
              textAlign: TextAlign.start,
              style: TextStyle(
                fontSize: 15,
                color: Colors.black,
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(left: 20),
            child: Text(
              "BS-EAR-H1i-GN = Garansi 2 Tahun",
              textAlign: TextAlign.start,
              style: TextStyle(
                fontSize: 15,
                color: Colors.black,
              ),
            ),
          ),
          SizedBox(
            height: 15,
          ),
          Container(
            margin: EdgeInsets.only(left: 20),
            child: Text(
              "Spesifikasi",
              textAlign: TextAlign.start,
              style: TextStyle(
                  fontSize: 17,
                  color: Colors.black,
                  fontWeight: FontWeight.bold),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(20),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(padding: EdgeInsets.only(right: 30)),
                Text(
                  "Bahan: ABS+PC+logam",
                  textAlign: TextAlign.start,
                  style: TextStyle(
                    fontSize: 15,
                    color: Colors.black,
                  ),
                ),
                Text(
                  "Bahan: ABS+PC+logam",
                  textAlign: TextAlign.start,
                  style: TextStyle(
                    fontSize: 15,
                    color: Colors.black,
                  ),
                ),
                Text(
                  "Jarak max: 10m",
                  textAlign: TextAlign.start,
                  style: TextStyle(
                    fontSize: 15,
                    color: Colors.black,
                  ),
                ),
                Text(
                  "Waktu play musik: 100 jam (volume 70%, ANC off) 70 jam (volume 70% ANC on)",
                  textAlign: TextAlign.start,
                  style: TextStyle(
                    fontSize: 15,
                    color: Colors.black,
                  ),
                ),
                Text(
                  "Kapasitas baterai: 600mAh/2,22Wh",
                  textAlign: TextAlign.start,
                  style: TextStyle(
                    fontSize: 15,
                    color: Colors.black,
                  ),
                ),
                Text(
                  "Pengisian daya: Tipe-C",
                  textAlign: TextAlign.start,
                  style: TextStyle(
                    fontSize: 15,
                    color: Colors.black,
                  ),
                ),
                Text(
                  "Waktu pengisian daya: Sekita 2 jam",
                  textAlign: TextAlign.start,
                  style: TextStyle(
                    fontSize: 15,
                    color: Colors.black,
                  ),
                ),
                Text(
                  "Rentang respons frekuensi: 20Hz hingga 20kHz",
                  textAlign: TextAlign.start,
                  style: TextStyle(
                    fontSize: 15,
                    color: Colors.black,
                  ),
                ),
                Text(
                  "Codec: LHDC/AAC/SBC",
                  textAlign: TextAlign.start,
                  style: TextStyle(
                    fontSize: 15,
                    color: Colors.black,
                  ),
                ),
              ],
            ),
          ),
        ]),
        bottomNavigationBar: Container(
          width: 160,
          height: 50,
          margin: const EdgeInsets.only(left: 250, bottom: 10, right: 10),
          child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                  backgroundColor: const Color(0xff2E2977),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10))),
              onPressed: () {
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (BuildContext context) => CheckoutView(
                      product:
                          'Baseus Bowie H1 Noise-Collection Wireless/Wired Headphone ANC Audio',
                      shortName: 'Headphone',
                      harga: 500000,
                      jumlah: _counter,
                      image: 'headphone.png',
                    ),
                  ),
                );
              },
              child: const Text(
                'Beli',
                style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.bold,
                    color: Colors.white),
              )),
        ));
  }
}
