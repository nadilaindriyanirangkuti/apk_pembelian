import 'package:flutter/material.dart';
import 'package:pembelian_mobileapk/Screen/checkout_screen.dart';
import 'package:pembelian_mobileapk/Widget/typography_widget.dart';

class IpadDetail extends StatefulWidget {
  const IpadDetail({super.key});

  @override
  State<IpadDetail> createState() => _IpadDetailState();
}

class _IpadDetailState extends State<IpadDetail> {
  int _counter = 1;

  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }

  void _decrementCounter() {
    setState(() {
      if (_counter > 0) {
        _counter--;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          scrolledUnderElevation: 0,
          centerTitle: true,
          title: Image.asset(
            'assets/image/appbar.png',
            width: 130,
            fit: BoxFit.fill,
          ),
        ),
        body: ListView(
          children: [
            Padding(
              padding: const EdgeInsets.all(16),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(20),
                child: SizedBox.fromSize(
                  size: const Size.fromRadius(100),
                  child:
                      Image.asset("assets/image/ipad.png", fit: BoxFit.contain),
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(left: 20),
              // padding: EdgeInsets.all(10),
              child: Text(
                "APPLE iPAD PRO 11inch M1 2TB RAM 16GB WIFI RESMI IBOX",
                style: TextStyle(
                  fontSize: 18,
                  color: Color(0xFF4C53A5),
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(left: 20, right: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "Rp. 6.900.000",
                    style: TextStyle(
                      fontSize: 16,
                      color: Color(0xffFF6B00),
                      fontWeight: FontWeight.normal,
                    ),
                  ),
                  Container(
                    width: 110,
                    decoration: BoxDecoration(
                      color: Color(0xFFDED6F1),
                      borderRadius: BorderRadius.circular(25),
                    ),
                    child: Row(
                      children: [
                        IconButton(
                          constraints:
                              BoxConstraints(minWidth: 0, minHeight: 0),
                          onPressed: _decrementCounter,
                          icon: Icon(
                            Icons.remove,
                          ),
                          iconSize: 16,
                        ),
                        Text(
                          '$_counter',
                          style: XTypo.font16W500(),
                        ),
                        IconButton(
                          constraints:
                              BoxConstraints(minWidth: 0, minHeight: 0),
                          onPressed: _incrementCounter,
                          icon: Icon(
                            Icons.add,
                          ),
                          iconSize: 16,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Container(
              margin: EdgeInsets.only(left: 20),
              child: Text(
                "Deskripsi",
                style: TextStyle(
                  fontSize: 20,
                  color: Color(0xFF4C53A5),
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            SizedBox(
              height: 12,
            ),
            Container(
              margin: EdgeInsets.only(left: 20),
              child: Text(
                "Layar: 11 inci Liquid Retina display dengan teknologi ProMotion, True Tone, dan rentang warna lebar (P3)",
                textAlign: TextAlign.start,
                style: TextStyle(
                  fontSize: 15,
                  color: Colors.black,
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(left: 20),
              child: Text(
                "Chip: Apple M1 chip yang menawarkan performa CPU dan GPU yang luar biasa serta Neural Engine yang canggih.",
                textAlign: TextAlign.start,
                style: TextStyle(
                  fontSize: 15,
                  color: Colors.black,
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(left: 20),
              child: Text(
                "Kapasitas Penyimpanan: 2TB.",
                textAlign: TextAlign.start,
                style: TextStyle(
                  fontSize: 15,
                  color: Colors.black,
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(left: 20),
              child: Text(
                "Memori RAM: 16GB.",
                textAlign: TextAlign.start,
                style: TextStyle(
                  fontSize: 15,
                  color: Colors.black,
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(left: 20),
              child: Text(
                "Kamera: Kamera belakang 12MP Wide dan 10MP Ultra Wide.",
                textAlign: TextAlign.start,
                style: TextStyle(
                  fontSize: 15,
                  color: Colors.black,
                ),
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Container(
              margin: EdgeInsets.only(left: 20),
              child: Text(
                "Kamera depan TrueDepth 12MP dengan fitur Portrait mode dan Animoji/Memoji.",
                textAlign: TextAlign.start,
                style: TextStyle(
                  fontSize: 15,
                  color: Colors.black,
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(left: 20),
              child: Text(
                "Video Recording: Perekaman video 4K hingga 60 fps.",
                textAlign: TextAlign.start,
                style: TextStyle(
                  fontSize: 15,
                  color: Colors.black,
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(left: 20),
              child: Text(
                "Audio: Empat speaker audio dengan suara stereo yang lebih luas.",
                textAlign: TextAlign.start,
                style: TextStyle(
                  fontSize: 15,
                  color: Colors.black,
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(left: 20),
              child: Text(
                "Konektivitas: Wi-Fi 6 (802.11ax) dan Bluetooth 5.0.",
                textAlign: TextAlign.start,
                style: TextStyle(
                  fontSize: 15,
                  color: Colors.black,
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(left: 20),
              child: Text(
                "Baterai: Hingga 10 jam penggunaan web pada Wi-Fi atau menonton video.",
                textAlign: TextAlign.start,
                style: TextStyle(
                  fontSize: 15,
                  color: Colors.black,
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(left: 20),
              child: Text(
                "Aksesori: Mendukung Apple Pencil generasi ke-2, Magic Keyboard, dan Smart Keyboard Folio.",
                textAlign: TextAlign.start,
                style: TextStyle(
                  color: Colors.black,
                ),
              ),
            ),
            SizedBox(
              height: 15,
            )
          ],
        ),
        bottomNavigationBar: Container(
          width: 160,
          height: 50,
          margin: const EdgeInsets.only(left: 250, bottom: 10, right: 10),
          child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                  backgroundColor: const Color(0xff2E2977),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10))),
              onPressed: () {
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (BuildContext context) => CheckoutView(
                      product:
                          'APPLE iPAD PRO 11inch M1 2TB RAM 16GB WIFI RESMI IBOX',
                      shortName: 'Ipad',
                      harga: 16900000,
                      jumlah: _counter,
                      image: 'ipad.png',
                    ),
                  ),
                );
              },
              child: const Text(
                'Beli',
                style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.bold,
                    color: Colors.white),
              )),
        ));
  }
}
