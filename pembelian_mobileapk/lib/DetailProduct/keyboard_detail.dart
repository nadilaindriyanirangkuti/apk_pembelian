import 'package:flutter/material.dart';
import 'package:pembelian_mobileapk/Screen/checkout_screen.dart';
import 'package:pembelian_mobileapk/Widget/typography_widget.dart';

class KeyboardDetail extends StatefulWidget {
  const KeyboardDetail({super.key});

  @override
  State<KeyboardDetail> createState() => _KeyboardDetailState();
}

class _KeyboardDetailState extends State<KeyboardDetail> {
  int _counter = 1;

  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }

  void _decrementCounter() {
    setState(() {
      if (_counter > 0) {
        _counter--;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
          elevation: 0,
          surfaceTintColor: Colors.white,
          forceMaterialTransparency: true,
          scrolledUnderElevation: 0.0,
          centerTitle: true,
          title: Image.asset(
            'assets/image/appbar.png',
            width: 130,
            fit: BoxFit.fill,
          )),
      body: ListView(
        children: [
          Padding(
            padding: const EdgeInsets.all(16),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(20),
              child: SizedBox.fromSize(
                size: const Size.fromRadius(100),
                child: Image.asset("assets/image/keyboard_rexus.png",
                    fit: BoxFit.contain),
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(left: 20),
            // padding: EdgeInsets.all(10),
            child: Text(
              "Rexus Keyboard Gaming Mini Battlefire K68M",
              style: TextStyle(
                fontSize: 20,
                color: Color(0xFF4C53A5),
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(left: 20, right: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "Rp. 210.000",
                  style: TextStyle(
                    fontSize: 16,
                    color: Color(0xffFF6B00),
                    fontWeight: FontWeight.normal,
                  ),
                ),
                Container(
                  width: 110,
                  decoration: BoxDecoration(
                    color: Color(0xFFDED6F1),
                    borderRadius: BorderRadius.circular(25),
                  ),
                  child: Row(
                    children: [
                      IconButton(
                        constraints: BoxConstraints(minWidth: 0, minHeight: 0),
                        onPressed: _decrementCounter,
                        icon: Icon(
                          Icons.remove,
                        ),
                        iconSize: 16,
                      ),
                      Text(
                        '$_counter',
                        style: XTypo.font16W500(),
                      ),
                      IconButton(
                        constraints: BoxConstraints(minWidth: 0, minHeight: 0),
                        onPressed: _incrementCounter,
                        icon: Icon(
                          Icons.add,
                        ),
                        iconSize: 16,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Container(
            margin: EdgeInsets.only(left: 20),
            child: Text(
              "Deskripsi",
              style: TextStyle(
                fontSize: 20,
                color: Color(0xFF4C53A5),
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          SizedBox(
            height: 12,
          ),
          Container(
            margin: EdgeInsets.only(left: 20),
            child: Text(
              "Keyboard Gaming Mini Battlefire K68M merupakan keyboard gaming membrane mini pertama dari Rexus dengan tampilan layout 65% berjumlah 68 tombol dengan 10 mode LED yang dapat diubah dengan kombinasi tombol FN yang sudah tertera di font keycaps. ",
              textAlign: TextAlign.start,
              style: TextStyle(
                fontSize: 15,
                color: Colors.black,
              ),
            ),
          ),
          SizedBox(
            height: 15,
          ),
          Container(
            margin: EdgeInsets.only(left: 20),
            child: Text(
              "Spesifikasi Produk",
              textAlign: TextAlign.start,
              style: TextStyle(
                  fontSize: 17,
                  color: Colors.black,
                  fontWeight: FontWeight.bold),
            ),
          ),
          SizedBox(
            height: 5,
          ),
          Container(
            margin: EdgeInsets.only(left: 20),
            child: Text(
              "Tipe Keyboard 	: Keyboard Gaming Mini Membrane",
              textAlign: TextAlign.start,
              style: TextStyle(
                fontSize: 15,
                color: Colors.black,
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(left: 20),
            child: Text(
              "LED : Lampu Latar RGB ",
              textAlign: TextAlign.start,
              style: TextStyle(
                fontSize: 15,
                color: Colors.black,
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(left: 20),
            child: Text(
              "Bahan Plate: Plate Metal Built-in 0.8mm ",
              textAlign: TextAlign.start,
              style: TextStyle(
                fontSize: 15,
                color: Colors.black,
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(left: 20),
            child: Text(
              "Kabel: Kabel Braided 1.5m & USB Lapis Emas  ",
              textAlign: TextAlign.start,
              style: TextStyle(
                fontSize: 15,
                color: Colors.black,
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(left: 20),
            child: Text(
              "Material	: Plastik ABS ",
              textAlign: TextAlign.start,
              style: TextStyle(
                fontSize: 15,
                color: Colors.black,
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(left: 20),
            child: Text(
              "Dimensi	: 310 * 103 * 25mm",
              textAlign: TextAlign.start,
              style: TextStyle(
                fontSize: 15,
                color: Colors.black,
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(left: 20),
            child: Text(
              "Kompatibilitas	: Windows OS / Mac OS ",
              textAlign: TextAlign.start,
              style: TextStyle(
                fontSize: 15,
                color: Colors.black,
              ),
            ),
          ),
        ],
      ),
      bottomNavigationBar: Container(
        width: 160,
        height: 50,
        margin: const EdgeInsets.only(left: 250, bottom: 10, right: 10),
        child: ElevatedButton(
            style: ElevatedButton.styleFrom(
                backgroundColor: const Color(0xff2E2977),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10))),
            onPressed: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (BuildContext context) => CheckoutView(
                    product: 'Rexus Keyboard Gaming Mini Battlefire K68M',
                    shortName: 'Keyboard',
                    harga: 210000,
                    jumlah: _counter,
                    image: 'keyboard_rexus.png',
                  ),
                ),
              );
            },
            child: const Text(
              'Beli',
              style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.bold,
                  color: Colors.white),
            )),
      ),
    );
  }
}
