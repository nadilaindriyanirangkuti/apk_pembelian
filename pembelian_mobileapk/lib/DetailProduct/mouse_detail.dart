import 'package:flutter/material.dart';
import 'package:pembelian_mobileapk/Screen/checkout_screen.dart';
import 'package:pembelian_mobileapk/Widget/typography_widget.dart';

class MouseDetail extends StatefulWidget {
  const MouseDetail({super.key});

  @override
  State<MouseDetail> createState() => _MouseDetailState();
}

class _MouseDetailState extends State<MouseDetail> {
  int _counter = 1;

  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }

  void _decrementCounter() {
    setState(() {
      if (_counter > 0) {
        _counter--;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          elevation: 0,
          surfaceTintColor: Colors.white,
          forceMaterialTransparency: true,
          scrolledUnderElevation: 0.0,
          centerTitle: true,
          title: Image.asset(
            'assets/image/appbar.png',
            width: 130,
            fit: BoxFit.fill,
          )),
      body: ListView(
        children: [
          Padding(
            padding: const EdgeInsets.all(16),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(20),
              child: SizedBox.fromSize(
                size: const Size.fromRadius(100),
                child:
                    Image.asset("assets/image/mouse.png", fit: BoxFit.contain),
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(left: 20),
            // padding: EdgeInsets.all(10),
            child: Text(
              "Mouse Gaming Rexus Shaga RX130 Charging Dock - Wireless RX 130 Gaming Mouse",
              style: TextStyle(
                fontSize: 18,
                color: Color(0xFF4C53A5),
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(left: 20, right: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "Rp. 310.000",
                  style: TextStyle(
                    fontSize: 16,
                    color: Color(0xffFF6B00),
                    fontWeight: FontWeight.normal,
                  ),
                ),
                Container(
                  width: 110,
                  decoration: BoxDecoration(
                    color: Color(0xFFDED6F1),
                    borderRadius: BorderRadius.circular(25),
                  ),
                  child: Row(
                    children: [
                      IconButton(
                        constraints: BoxConstraints(minWidth: 0, minHeight: 0),
                        onPressed: _decrementCounter,
                        icon: Icon(
                          Icons.remove,
                        ),
                        iconSize: 16,
                      ),
                      Text(
                        '$_counter',
                        style: XTypo.font16W500(),
                      ),
                      IconButton(
                        constraints: BoxConstraints(minWidth: 0, minHeight: 0),
                        onPressed: _incrementCounter,
                        icon: Icon(
                          Icons.add,
                        ),
                        iconSize: 16,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Container(
            margin: EdgeInsets.only(left: 20),
            child: Text(
              "Deskripsi",
              style: TextStyle(
                fontSize: 20,
                color: Color(0xFF4C53A5),
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          SizedBox(
            height: 12,
          ),
          Container(
            margin: EdgeInsets.only(left: 20),
            child: Text(
              "Mouse Gaming Rexus Shaga RX130 Charging Dock - Wireless RX 130 Gaming Mouse",
              textAlign: TextAlign.start,
              style: TextStyle(
                fontSize: 16,
                color: Colors.black,
              ),
            ),
          ),
          SizedBox(
            height: 8,
          ),
          Container(
            margin: EdgeInsets.only(left: 20),
            child: Text(
              "Garansi Resmi 1 Tahun",
              textAlign: TextAlign.start,
              style: TextStyle(
                fontSize: 17,
                color: Colors.black,
              ),
            ),
          ),
          SizedBox(
            height: 15,
          ),
          Container(
            margin: EdgeInsets.only(left: 20),
            child: Text(
              "Spesifikasi Produk",
              textAlign: TextAlign.start,
              style: TextStyle(
                  fontSize: 17,
                  color: Colors.black,
                  fontWeight: FontWeight.bold),
            ),
          ),
          Container(
            margin: EdgeInsets.only(left: 20),
            child: Text(
              "DPI : 800 - 1200 - 1600 - 2400 - 5000 - 10000",
              textAlign: TextAlign.start,
              style: TextStyle(
                fontSize: 17,
                color: Colors.black,
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(left: 20),
            child: Text(
              "Konektivitas: 2.4G + Bluetooth",
              textAlign: TextAlign.start,
              style: TextStyle(
                fontSize: 17,
                color: Colors.black,
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(left: 20),
            child: Text(
              "Port : USB",
              textAlign: TextAlign.start,
              style: TextStyle(
                fontSize: 17,
                color: Colors.black,
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(left: 20),
            child: Text(
              "Baterai: Lithium 600mAh",
              textAlign: TextAlign.start,
              style: TextStyle(
                fontSize: 17,
                color: Colors.black,
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(left: 20),
            child: Text(
              "Tegangan / Arus: 3.7V - 25mA (Mouse), 5V-500mA (Dock pengisi daya)",
              textAlign: TextAlign.start,
              style: TextStyle(
                fontSize: 17,
                color: Colors.black,
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(left: 20),
            child: Text(
              "Berat : 73.5g (Mouse), 62.5g (Dock pengisi daya)",
              textAlign: TextAlign.start,
              style: TextStyle(
                fontSize: 17,
                color: Colors.black,
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(left: 20),
            child: Text(
              "Suhu Operasi : 0°C ke +40°C",
              textAlign: TextAlign.start,
              style: TextStyle(
                fontSize: 17,
                color: Colors.black,
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(left: 20),
            child: Text(
              "Suhu Penyimpanan : 0°C ke +40°C",
              textAlign: TextAlign.start,
              style: TextStyle(
                fontSize: 17,
                color: Colors.black,
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(left: 20),
            child: Text(
              "Kekuatan Operasi: 60-80gf",
              textAlign: TextAlign.start,
              style: TextStyle(
                fontSize: 17,
                color: Colors.black,
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(left: 20),
            child: Text(
              "Panjang Kabel Paracord : 1.8M",
              textAlign: TextAlign.start,
              style: TextStyle(
                fontSize: 17,
                color: Colors.black,
              ),
            ),
          ),
          SizedBox(
            height: 50,
          )
        ],
      ),
      bottomNavigationBar: Container(
        width: 160,
        height: 50,
        margin: const EdgeInsets.only(left: 250, bottom: 10, right: 10),
        child: ElevatedButton(
            style: ElevatedButton.styleFrom(
                backgroundColor: const Color(0xff2E2977),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10))),
            onPressed: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (BuildContext context) => CheckoutView(
                    product:
                        'Mouse Gaming Rexus Shaga RX130 Charging Dock - Wireless RX 130 Gaming Mouse',
                    shortName: 'Mouse',
                    harga: 310000,
                    jumlah: _counter,
                    image: 'mouse.png',
                  ),
                ),
              );
            },
            child: const Text(
              'Beli',
              style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.bold,
                  color: Colors.white),
            )),
      ),
    );
  }
}
