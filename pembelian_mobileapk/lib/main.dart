import 'package:flutter/material.dart';
import 'package:pembelian_mobileapk/Moduls/check_saldo_provider.dart';
import 'package:pembelian_mobileapk/Moduls/http_provider.dart';
import 'package:pembelian_mobileapk/Moduls/product_provider.dart';
import 'package:pembelian_mobileapk/Moduls/top_up_saldo_provider.dart';
import 'package:pembelian_mobileapk/Screen/home_screen.dart';
import 'package:provider/provider.dart';
import 'package:intl/date_symbol_data_local.dart';

void main() {
  initializeDateFormatting('id');
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => HttpProvider()),
        ChangeNotifierProvider(create: (_) => TopUpSaldoProvider()),
        ChangeNotifierProvider(create: (_) => CheckSaldoProvider()),
        ChangeNotifierProvider(create: (_) => ProductProvider()),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Flutter Demo',
        theme: ThemeData(
          colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
          useMaterial3: true,
        ),
        home: HomeScreen(),
      ),
    );
  }
}


// class SplashScreenPage extends StatelessWidget {
//   Future<void> _loadResources() async {
//     await Future.delayed(const Duration(seconds: 5)); 
//   }

//   const SplashScreenPage({super.key});

//   @override
//   Widget build(BuildContext context) {
//     return FutureBuilder(
//       future: _loadResources(),
//       builder: (context, snapshot) {
//          if (snapshot.connectionState == ConnectionState.waiting) {
//           return Scaffold(
//             body: Center(
//               child: Image.asset('assets/image/splash_screen.png'),
//             ),
//           );
//         } else {
//           // return const HomeScreen();
//           return const HomeScreen();
//         }
//       },
//     );
//   }
// }



// class SplashScreen extends StatefulWidget {
//   const SplashScreen({super.key});

//   @override
//   State<SplashScreen> createState() => _SplashScreenState();
// }

// class _SplashScreenState extends State<SplashScreen> {

//   // @override
//   // void initState() {
//   //   super.initState();
//   //   _navigateToHome();
//   // }
//   Future<void> _loadResources() async {
//     await Future.delayed(Duration(seconds: 3)); // Simulasi pemuatan data
//   }

// _navigateToHome() async {
//     await Future.delayed(Duration(seconds: 3), () {
//       print('navigation');
//     }); // Tunda selama 3 detik
//     Navigator.pushReplacement(
//       context,
//       MaterialPageRoute(
//         builder: (context) =>
//             HomeScreen(), // Ganti dengan layar utama aplikasi Anda
//       ),
//     );
//   }

//   @override
//   Widget build(BuildContext context) {
//     return SplashScreen();
//   }
// }


