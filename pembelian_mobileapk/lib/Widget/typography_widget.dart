import 'package:flutter/material.dart';

class XTypo {
  static TextStyle font20W600() => const TextStyle(
        fontSize: 20,
        fontWeight: FontWeight.w600,
        color: Color(0xFF000000),
      );
  static TextStyle font18Normal() => const TextStyle(
        fontSize: 18,
        fontWeight: FontWeight.normal,
        color: Color(0xFF000000),
      );
  static TextStyle font18Bold() => const TextStyle(
        fontSize: 18,
        fontWeight: FontWeight.bold,
        color: Color(0xFF000000),
      );
  static TextStyle font17W500() => const TextStyle(
        fontSize: 17,
        fontWeight: FontWeight.w500,
        height: 1.2,
        color: Color(0xFF000000),
      );
  static TextStyle font16Bold() => const TextStyle(
        fontSize: 16,
        fontWeight: FontWeight.bold,
        color: Color(0xFF000000),
      );
  static TextStyle font16Normal() => const TextStyle(
        fontSize: 16,
        fontWeight: FontWeight.normal,
        color: Color(0xFF000000),
      );
  static TextStyle font16W400() => const TextStyle(
        fontSize: 16,
        fontWeight: FontWeight.w400,
        height: 1.2,
        color: Color(0xFF000000),
      );
  static TextStyle font16W500() => const TextStyle(
        fontSize: 16,
        fontWeight: FontWeight.w500,
        height: 1.5,
        color: Color(0xFF000000),
      );

  static TextStyle font16W700() => const TextStyle(
        fontSize: 16,
        fontWeight: FontWeight.w700,
        height: 1.2,
        color: Color(0xFFFFFFFF),
      );

  static TextStyle font15W600() => const TextStyle(
        fontSize: 15,
        fontWeight: FontWeight.w600,
        height: 1.2,
        color: Color(0xFF000000),
      );

  static TextStyle font15W500({Color? color}) => TextStyle(
        fontSize: 15,
        fontWeight: FontWeight.w500,
        height: 1.2,
        color: color,
      );
}
