import 'package:flutter/material.dart';
import 'package:pembelian_mobileapk/Util/date_util.dart';
import 'package:pembelian_mobileapk/Widget/typography_widget.dart';
import 'package:pembelian_mobileapk/Util/string_util.dart';

class DialogSuccessTopUpWidget extends StatelessWidget {
  final Map<String, dynamic> data;
  final int nominal;
  final DateTime date;
  const DialogSuccessTopUpWidget({
    super.key,
    required this.data,
    required this.nominal,
    required this.date,
  });

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
      child: Padding(
        padding: EdgeInsets.all(20),
        child: SingleChildScrollView(
          child: ListBody(
            children: <Widget>[
              Container(
                color: Colors.transparent,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(right: 10),
                      child: Image.asset(
                        'assets/image/Berhasil.png',
                        width: 150,
                      ),
                    ),
                    const SizedBox(height: 15),
                    Text(
                      "Top Up Saldo Berhasil",
                      style: const TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    Text(
                      "${date.fullDate} WIB",
                      style: TextStyle(
                        color: Colors.grey[600],
                        fontSize: 15,
                        fontWeight: FontWeight.normal,
                      ),
                    ),
                    const SizedBox(height: 15),
                    Text(
                      nominal.currency,
                      style: TextStyle(
                        fontSize: 25,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
              ),
              const SizedBox(height: 15),
              Container(
                width: double.infinity, // Memenuhi lebar layar
                height: 1.0, // Tinggi garis
                color: Color(0xFFDED6F1), // Warna garis
              ),
              const SizedBox(height: 30),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'Penerima',
                    style: XTypo.font16Normal(),
                  ),
                  Text(
                    '${data['nama']}',
                    style: XTypo.font16Bold(),
                  ),
                ],
              ),
              const SizedBox(height: 10),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'Bank Penerima',
                    style: XTypo.font16Normal(),
                  ),
                  Text(
                    '${data['nama_bank']}',
                    style: XTypo.font16Bold(),
                  ),
                ],
              ),
              const SizedBox(height: 10),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'Nomor Rekengin',
                    style: XTypo.font16Normal(),
                  ),
                  Text(
                    '${data['norek']}',
                    style: XTypo.font16Bold(),
                  ),
                ],
              ),
              const SizedBox(height: 10),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'Saldo',
                    style: XTypo.font16Normal(),
                  ),
                  Text(
                    ((data['saldo'] as double).toInt()).currency,
                    style: XTypo.font16Bold(),
                  ),
                ],
              ),
              SizedBox(height: 30),
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                  backgroundColor: const Color(0xff2E2977),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10),
                  ),
                ),
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: const Text(
                  'Oke',
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
