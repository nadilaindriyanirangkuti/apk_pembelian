import 'package:flutter/material.dart';
import 'package:pembelian_mobileapk/Screen/home_screen.dart';
import 'package:pembelian_mobileapk/Util/date_util.dart';
import 'package:pembelian_mobileapk/Widget/typography_widget.dart';
import 'package:pembelian_mobileapk/Util/string_util.dart';

class DialogSuccessTransaksiWidget extends StatelessWidget {
  final Map<String, dynamic> data;
  final DateTime date;
  const DialogSuccessTransaksiWidget({
    super.key,
    required this.data,
    required this.date,
  });

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
      child: Padding(
        padding: EdgeInsets.all(20),
        child: SingleChildScrollView(
          child: ListBody(
            children: <Widget>[
              Container(
                color: Colors.transparent,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(right: 10),
                      child: Image.asset(
                        'assets/image/Berhasil.png',
                        width: 150,
                      ),
                    ),
                    const SizedBox(height: 15),
                    Text(
                      "Transaksi Berhasil",
                      style: const TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    Text(
                      "${date.fullDate} WIB",
                      style: TextStyle(
                        color: Colors.grey[600],
                        fontSize: 15,
                        fontWeight: FontWeight.normal,
                      ),
                    ),
                    const SizedBox(height: 15),
                    Text(
                      (data['total_bayar'] as int).currency,
                      style: TextStyle(
                        fontSize: 25,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
              ),
              const SizedBox(height: 15),
              Container(
                width: double.infinity, // Memenuhi lebar layar
                height: 1.0, // Tinggi garis
                color: Color(0xFFDED6F1), // Warna garis
              ),
              const SizedBox(height: 30),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'Product',
                    style: XTypo.font16Normal(),
                  ),
                  Text(
                    '${data['short_name']}',
                    style: XTypo.font16Bold(),
                  ),
                ],
              ),
              const SizedBox(height: 5),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'Jumlah',
                    style: XTypo.font16Normal(),
                  ),
                  Text(
                    '${data['jumlah']}',
                    style: XTypo.font16Bold(),
                  ),
                ],
              ),
              const SizedBox(height: 15),
              Container(
                width: double.infinity, // Memenuhi lebar layar
                height: 1.0, // Tinggi garis
                color: Color(0xFFDED6F1), // Warna garis
              ),
              const SizedBox(height: 15),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'Total Harga',
                    style: XTypo.font16Normal(),
                  ),
                  Text(
                    (data['total_harga'] as int).currency,
                    style: XTypo.font16Bold(),
                  ),
                ],
              ),
              const SizedBox(height: 10),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'Ongkos Kirim',
                    style: XTypo.font16Normal(),
                  ),
                  Text(
                    (data['ongkir'] as int).currency,
                    style: XTypo.font16Bold(),
                  ),
                ],
              ),
              const SizedBox(height: 30),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'Total Bayar',
                    style: XTypo.font16Normal(),
                  ),
                  Text(
                    (data['total_bayar'] as int).currency,
                    style: XTypo.font16Bold(),
                  ),
                ],
              ),
              const SizedBox(height: 10),
              SizedBox(height: 30),
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                  backgroundColor: const Color(0xff2E2977),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10),
                  ),
                ),
                onPressed: () {
                  Navigator.of(context).pop();
                  Navigator.of(context).pushAndRemoveUntil(
                    MaterialPageRoute(
                      builder: (BuildContext context) => HomeScreen(),
                    ),
                    (Route<dynamic> route) => false,
                  );
                },
                child: const Text(
                  'OKE',
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
