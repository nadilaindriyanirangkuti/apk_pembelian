import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pembelian_mobileapk/Widget/typography_widget.dart';

class TextFormFieldBorder extends StatelessWidget {
  final String label;
  final TextEditingController textController;
  final String hintText;
  final String? messageValidator;
  const TextFormFieldBorder({
    super.key,
    required this.label,
    required this.textController,
    required this.hintText,
    this.messageValidator,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          label,
          style: XTypo.font20W600(),
        ),
        const SizedBox(height: 4),
        TextFormField(
          // Assign the controller
          controller: textController,
          style: const TextStyle(color: Colors.black, fontSize: 15.0),
          onChanged: (value) {},
          decoration: InputDecoration(
            contentPadding: const EdgeInsets.symmetric(
              vertical: 10,
              horizontal: 16,
            ),
            hintText: hintText,
            hintStyle: const TextStyle(
              fontSize: 17,
              fontWeight: FontWeight.normal,
              color: Colors.grey,
            ),
            border: const OutlineInputBorder(),
            enabledBorder: const OutlineInputBorder(
              borderSide: BorderSide(
                color: Color(0xFF2E2977),
                width: 1,
              ),
            ),
          ),
          validator: (value) {
            if (value == null || value.isEmpty) {
              return messageValidator;
            }
            return null;
          },
          inputFormatters: [
            FilteringTextInputFormatter.digitsOnly,
          ],
        ),
      ],
    );
  }
}
