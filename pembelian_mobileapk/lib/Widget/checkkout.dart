import 'package:flutter/material.dart';

class CheckoutPage extends StatefulWidget {
  const CheckoutPage({super.key});

  @override
  State<CheckoutPage> createState() => _CheckoutPageState();
}

class _CheckoutPageState extends State<CheckoutPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          scrolledUnderElevation: 0,
          elevation: 0,
          title: Image.asset(
            'assets/image/appbar.png',
            width: 300,
            height: 170,
          ),
        ),
        body: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              padding: const EdgeInsets.all(10),
              child: Row(
                children: [
                  Image.asset(
                    'assets/image/headphone.png',
                    fit: BoxFit.contain,
                    width: 180,
                  ),
                ],
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              margin: const EdgeInsets.only(left: 20),
              child: const Text(
                'Ringkasan Pembayaran',
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.w700),
              ),
            ),
            const Divider(
              indent: 20,
              endIndent: 20,
            ),
            const SizedBox(
              height: 30,
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              margin: const EdgeInsets.only(left: 20),
              child: const Row(
                children: [
                  Text(
                    'harga',
                    style: TextStyle(fontSize: 20),
                  ),
                  SizedBox(
                    width: 170,
                  ),
                  Text(
                    'Rp. 500.0000',
                    style: TextStyle(fontSize: 20),
                  )
                ],
              ),
            ),
            const SizedBox(
              height: 5,
            ),
            Container(
                width: MediaQuery.of(context).size.width,
                margin: const EdgeInsets.only(left: 20),
                child: const Row(
                  children: [
                    Text(
                      'Ongkos Kirim',
                      style: TextStyle(
                        fontSize: 20,
                      ),
                    ),
                    SizedBox(
                      width: 100,
                    ),
                    Text(
                      'Rp. 10.000',
                      style: TextStyle(fontSize: 20),
                    )
                  ],
                )),
            const SizedBox(
              height: 70,
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              margin: const EdgeInsets.only(left: 20),
              child: const Row(
                children: [
                  Text(
                    'Total Pembayaran',
                    style: TextStyle(fontSize: 20),
                  ),
                  SizedBox(
                    width: 60,
                  ),
                  Text(
                    'Rp. 500.0000',
                    style: TextStyle(fontSize: 20),
                  )
                ],
              ),
            ),
            const SizedBox(
              height: 30,
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              height: 60,
              padding: const EdgeInsets.only(left: 15, right: 15),
              margin: const EdgeInsets.symmetric(vertical: 8, horizontal: 10),
              decoration: BoxDecoration(
                  color: const Color(0xffDED6F1),
                  boxShadow: const [
                    BoxShadow(
                      blurRadius: 0,
                    )
                  ],
                  borderRadius: BorderRadius.circular(10)),
              child: Row(
                children: [
                  InkWell(
                    onTap: () {},
                    child: Container(
                      margin: const EdgeInsets.all(10),
                      child: Image.asset(
                        'assets/image/BNI.png',
                        width: 70,
                        height: 100,
                      ),
                    ),
                  ),
                  const SizedBox(
                    width: 130,
                  ),
                  Container(
                    child: const Text(
                      '*********2109',
                      style: TextStyle(fontSize: 18),
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
        bottomNavigationBar: Container(
          width: 160,
          height: 50,
          margin: const EdgeInsets.only(left: 210, bottom: 10, right: 10),
          child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                  backgroundColor: const Color(0xff2E2977),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10))),
              onPressed: () {
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (BuildContext context) => const CheckoutPage()));
              },
              child: const Text(
                'Buat Pesanan',
                style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                    color: Colors.white),
              )),
        ));
  }
}
