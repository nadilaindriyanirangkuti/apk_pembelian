import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:pembelian_mobileapk/Screen/dialog_screen.dart';
import 'package:pembelian_mobileapk/Widget/dialog_failed_widget.dart';

class CheckSaldoProvider with ChangeNotifier {
  final formKey = GlobalKey<FormState>();

  final TextEditingController norekController = TextEditingController();

  Map<String, dynamic> _data = {};
  Map<String, dynamic> get data => _data;

  Future<Map<String, dynamic>> checkSaldo(String norek) async {
    Uri url =
        Uri.parse("https://api.siunbin.com/api/v1/bank/rekening/saldo/$norek");

    var response = await http.get(url);
    if (response.statusCode == 200) {
      return jsonDecode(response.body);
    } else {
      throw Exception('Failed to load data');
    }
  }

  void submitForm(BuildContext context, Map<String, dynamic> payload) async {
    if (formKey.currentState?.validate() ?? false) {
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(content: Text('Sedang memproses...')),
      );
      try {
        Map<String, dynamic> response = await checkSaldo(payload['norek']);
        _data = response['data'];
        notifyListeners();
        print('datadata: $_data');
      } catch (e) {
        _data = {};
        notifyListeners();

        if (!context.mounted) return;
        _showFailedDialog(context, 'Rekening Tidak Terdaftar');
      }
    } else {
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(content: Text('Silakan isi semua field diatas.')),
      );
    }
  }

  void _showDialog(BuildContext context, bool isSuccess, String message) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return DialogScreen(
          isSuccess: isSuccess,
          message: message,
        );
      },
    );
  }

  void _showFailedDialog(BuildContext context, String message) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return DialogFailedWidget(
          message: message,
        );
      },
    );
  }
}
