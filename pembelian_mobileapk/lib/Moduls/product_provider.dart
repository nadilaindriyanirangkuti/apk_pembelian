import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:pembelian_mobileapk/Screen/dialog_screen.dart';
import 'package:pembelian_mobileapk/Widget/dialog_failed_widget.dart';
import 'package:pembelian_mobileapk/Widget/dialog_success_top_up_widget.dart';

class ProductProvider with ChangeNotifier {
  Map<String, dynamic> product = {};

  void setProduct(
      String nama, String shortName, int harga, int jumlah, String image) {
    int totalHarga = harga * jumlah;
    int totalBayar = totalHarga + 10000;

    product = {
      'nama': nama,
      'short_name': shortName,
      'harga': harga,
      'jumlah': jumlah,
      'total_harga': totalHarga,
      'image': image,
      'ongkir': 10000,
      'total_bayar': totalBayar,
    };

    notifyListeners();
  }
}
