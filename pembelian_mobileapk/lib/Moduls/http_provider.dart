import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:pembelian_mobileapk/Widget/dialog_failed_widget.dart';
import 'package:pembelian_mobileapk/Widget/dialog_success_transaksi_widget.dart';

class HttpProvider with ChangeNotifier {
  List<dynamic> _data = [];
  List<dynamic> get data => _data;

  Map<String, dynamic> _saldoRekening = {};
  Map<String, dynamic> get saldoRekening => _saldoRekening;

  String norekChecked = '';
  String namaBankChecked = '';

  String norekValidationForm = '';
  String pinkValidationForm = '';

  final TextEditingController norekController = TextEditingController();
  final TextEditingController pinController = TextEditingController();

  final formKey = GlobalKey<FormState>();

  Future<void> checkRekening() async {
    norekChecked = '';
    namaBankChecked = '';
    notifyListeners();
    Uri url =
        Uri.parse("https://api.siunbin.com/api/v1/bank/rekening/PEMBELIAN");

    var response = await http.get(url);

    if (response.statusCode == 200) {
      final Map<String, dynamic> responseData = jsonDecode(response.body);
      _data = responseData['data']; // Akses 'data' sebagai List<dynamic>
      addIsDisabledField(_data);
      notifyListeners();
    } else {
      throw Exception('Failed to load data');
    }
  }

  Future<Map<String, dynamic>> checkSaldo(String norek) async {
    Uri url =
        Uri.parse("https://api.siunbin.com/api/v1/bank/rekening/saldo/$norek");

    var response = await http.get(url);
    if (response.statusCode == 200) {
      return jsonDecode(response.body);
    } else {
      throw Exception('Failed to load data');
    }
  }

  Future<Map<String, dynamic>> transaksi(Map<String, dynamic> payload) async {
    Uri url = Uri.parse(
      "https://api.siunbin.com/api/v1/bank/transaksi/proses/${payload['norek']}/${payload['pin']}/${payload['norek_tujuan']}/${payload['nominal']}/${payload['keterangan']}",
    );
    var response = await http.get(url);
    if (response.statusCode == 200) {
      return jsonDecode(response.body);
    } else {
      throw Exception('Failed to load data');
    }
  }

  void submitForm(BuildContext context, Map<String, dynamic> payload) async {
    print('payload: $payload');
    if (formKey.currentState?.validate() ?? false) {
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(content: Text('Sedang memproses...')),
      );

      try {
        Map<String, dynamic> response = await checkSaldo(payload['norek']);

        if (payload['nominal'] <=
            double.parse('${response['data']['saldo']}').toInt()) {
          await transaksi(payload);

          DateTime now = DateTime.now();

          if (!context.mounted) return;
          _showSuccessDialog(context, payload, now);
        } else {
          if (!context.mounted) return;
          _showFailedDialog(context, 'Saldo Anda Tidak Cukup');
        }
      } catch (e) {
        _showFailedDialog(context, 'Rekening Tidak Terdaftar');
      }
    } else {
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(content: Text('Silakan isi semua field diatas.')),
      );
    }
  }

  void addIsDisabledField(List<dynamic> data) {
    data.forEach((element) {
      element['isChecked'] = false;
    });
  }

  void onTapCard(int index) {
    _data.forEach((element) {
      element['isChecked'] = false;
    });
    _data[index]['isChecked'] = true;
    norekChecked = _data[index]['norek'];
    namaBankChecked = _data[index]['nama_bank'];

    notifyListeners();
  }

  void _showSuccessDialog(
      BuildContext context, Map<String, dynamic> data, DateTime date) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return DialogSuccessTransaksiWidget(
          data: data,
          date: date,
        );
      },
    );
  }

  void _showFailedDialog(BuildContext context, String message) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return DialogFailedWidget(
          message: message,
        );
      },
    );
  }
}
