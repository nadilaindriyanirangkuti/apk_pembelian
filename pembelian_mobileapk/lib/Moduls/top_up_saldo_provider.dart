import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:pembelian_mobileapk/Screen/dialog_screen.dart';
import 'package:pembelian_mobileapk/Widget/dialog_failed_widget.dart';
import 'package:pembelian_mobileapk/Widget/dialog_success_top_up_widget.dart';

class TopUpSaldoProvider with ChangeNotifier {
  final formKey = GlobalKey<FormState>();

  final TextEditingController norekController = TextEditingController();
  final TextEditingController pinController = TextEditingController();
  final TextEditingController nominalController = TextEditingController();

  Future<Map<String, dynamic>> topUpSaldo(
      BuildContext context, Map<String, dynamic> payload) async {
    Uri url = Uri.parse(
        "https://api.siunbin.com/api/v1/bank/rekening/topup/${payload['norek']}/${payload['pin']}/${payload['nominal']}");

    var response = await http.get(url);
    if (response.statusCode == 200) {
      return jsonDecode(response.body);
    } else {
      throw Exception('Failed to load data');
    }
  }

  Future<Map<String, dynamic>> checkSaldo(String norek) async {
    Uri url =
        Uri.parse("https://api.siunbin.com/api/v1/bank/rekening/saldo/$norek");

    var response = await http.get(url);
    if (response.statusCode == 200) {
      return jsonDecode(response.body);
    } else {
      throw Exception('Failed to load data');
    }
  }

  void submitForm(BuildContext context, Map<String, dynamic> payload) async {
    if (formKey.currentState?.validate() ?? false) {
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(content: Text('Sedang memproses...')),
      );

      try {
        await topUpSaldo(context, payload);

        Map<String, dynamic> response = await checkSaldo(payload['norek']);

        DateTime now = DateTime.now();

        if (!context.mounted) return;
        _showSuccessDialog(context, response['data'], payload['nominal'], now);
      } catch (e) {
        _showFailedDialog(context, 'Rekening Tidak Terdaftar');
      }
    } else {
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(content: Text('Silakan isi semua field diatas.')),
      );
    }
  }

  void _showSuccessDialog(BuildContext context, Map<String, dynamic> data,
      int nominal, DateTime date) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return DialogSuccessTopUpWidget(
          data: data,
          nominal: nominal,
          date: date,
        );
      },
    );
  }

  void _showFailedDialog(BuildContext context, String message) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return DialogFailedWidget(
          message: message,
        );
      },
    );
  }
}
